import cmath
import healpy as hp
import itertools
import matplotlib
#matplotlib.use("Agg")
from matplotlib import cm as cm
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib import pyplot as plt
import numpy as np
from scipy.special import logsumexp
import time

from glue.ligolw import ligolw
from glue.ligolw import lsctables
from glue.ligolw import utils as ligolw_utils
from lal import series as lalseries
from lal import GreenwichMeanSiderealTime, ComputeDetAMResponse, C_SI
from lal import MSUN_SI, PC_SI, CreateCOMPLEX16FrequencySeries
import lalsimulation



@lsctables.use_in
class ContentHandler(lalseries.PSDContentHandler):
	pass
	
round = lambda x: int((2*x+1)//2)

start = time.time()

#gstlal/gstlal-inspiral/python/cbc_template_fir -> generate_template()
#lalsim.SimInspiralFD()
def generate_template(template_bank_row, approximant, sample_rate, duration, f_low, f_high, amporder = 0, order = 7, fwdplan = None, fworkspace = None):
	"""
	Generate a single frequency-domain template, which
	1. is band-limited between f_low and f_high,
	2. has an IFFT which is duration seconds long and
	3. has an IFFT which is sampled at sample_rate Hz
	"""

	assert f_high <= sample_rate // 2

	# FIXME use hcross somday?
	# We don't here because it is not guaranteed to be orthogonal
	# and we add orthogonal phase later

	parameters = {}
	parameters['m1'] = MSUN_SI * template_bank_row.mass1
	parameters['m2'] = MSUN_SI * template_bank_row.mass2
	parameters['S1x'] = template_bank_row.spin1x
	parameters['S1y'] = template_bank_row.spin1y
	parameters['S1z'] = template_bank_row.spin1z
	parameters['S2x'] = template_bank_row.spin2x
	parameters['S2y'] = template_bank_row.spin2y
	parameters['S2z'] = template_bank_row.spin2z
	parameters['distance'] = 1.e6 * PC_SI
	parameters['inclination'] = 0.
	parameters['phiRef'] = 0.
	parameters['longAscNodes'] = 0.
	parameters['eccentricity'] = 0.
	parameters['meanPerAno'] = 0.
	parameters['deltaF'] = 1.0 / duration
	parameters['f_min'] = f_low
	parameters['f_max'] = f_high
	parameters['f_ref'] = 0.
	parameters['LALparams'] = None
	parameters['approximant'] = lalsimulation.GetApproximantFromString(str(approximant))

	hplus, hcross = lalsimulation.SimInspiralFD(**parameters)
	assert len(hplus.data.data) == int(round(f_high * duration)) + 1
	# pad the output vector if the sample rate was higher than the
	# requested final frequency
	if f_high < sample_rate / 2:
		fseries = CreateCOMPLEX16FrequencySeries(
			name = hplus.name,
			epoch = hplus.epoch,
			f0 = hplus.f0,
			deltaF = hplus.deltaF,
			length = int(round(sample_rate * duration))//2 +1,
			sampleUnits = hplus.sampleUnits
		)
		fseries.data.data = numpy.zeros(fseries.data.length)
		fseries.data.data[:hplus.data.length] = hplus.data.data[:]
		hplus = fseries

		fseries = CreateCOMPLEX16FrequencySeries(
			name = hcross.name,
			epoch = hcross.epoch,
			f0 = hcross.f0,
			deltaF = hcross.deltaF,
			length = int(round(sample_rate * duration))//2 +1,
			sampleUnits = hcross.sampleUnits
		)
		fseries.data.data = numpy.zeros(fseries.data.length)
		fseries.data.data[:hcross.data.length] = hcross.data.data[:]
		hcross = fseries
	return hplus, hcross

# Development/gstlal/gstlal-inspiral/python/cbc_template_fir.py
# templates_workspace.make_whitened_template in claster
class templates_workspace(object):
	def __init__(self, template_table, approximant, psd, f_low, time_slices, autocorrelation_length = None, fhigh = None):
		self.template_table = template_table
		self.approximant = approximant
		self.f_low = f_low
		self.time_slices = time_slices
		self.autocorrelation_length = autocorrelation_length
		self.fhigh = fhigh
		self.sample_rate_max = max(time_slices["rate"])
		self.duration = max(time_slices["end"])
		self.length_max = int(round(self.duration * self.sample_rate_max))

		if self.fhigh is None:
			self.fhigh = self.sample_rate_max / 2.
		# Some input checking to avoid incomprehensible error messages
		if not self.template_table:
			raise ValueError("template list is empty")
		if self.f_low < 0.:
			raise ValueError("f_low must be >= 0. %s" % repr(self.f_low))

		# working f_low to actually use for generating the waveform.  pick
		# template with lowest chirp mass, compute its duration starting
		# from f_low;  the extra time is 10% of this plus 3 cycles (3 /
		# f_low);  invert to obtain f_low corresponding to desired padding.
		# NOTE:  because SimInspiralChirpStartFrequencyBound() does not
		# account for spin, we set the spins to 0 in the call to
		# SimInspiralChirpTimeBound() regardless of the component's spins.
		template = min(self.template_table, key = lambda row: row.mchirp)
		tchirp = lalsim.SimInspiralChirpTimeBound(self.f_low, template.mass1 * lal.MSUN_SI, template.mass2 * lal.MSUN_SI, 0., 0.)
		working_f_low = lalsim.SimInspiralChirpStartFrequencyBound(1.1 * tchirp + 3. / self.f_low, template.mass1 * lal.MSUN_SI, template.mass2 * lal.MSUN_SI)

		# Add duration of PSD to template length for PSD ringing, round up to power of 2 count of samples
		self.working_length = templates.ceil_pow_2(self.length_max + round(1./psd.deltaF * self.sample_rate_max))
		self.working_duration = float(self.working_length) / self.sample_rate_max

		# Smooth the PSD and interpolate to required resolution
		if not FIR_WHITENER and psd is not None:
			self.psd = condition_psd(psd, 1.0 / self.working_duration, minfs = (working_f_low, self.f_low), maxfs = (self.sample_rate_max / 2.0 * 0.90, self.sample_rate_max / 2.0))
		else:
			self.psd = reference_psd.interpolate_psd(psd, 1.0 / self.working_duration)
		self.revplan = lal.CreateReverseCOMPLEX16FFTPlan(self.working_length, 1)
		self.fwdplan = lal.CreateForwardREAL8FFTPlan(self.working_length, 1)
		self.tseries = lal.CreateCOMPLEX16TimeSeries(
			name = "timeseries",
			epoch = LIGOTimeGPS(0.),
			f0 = 0.,
			deltaT = 1.0 / self.sample_rate_max,
			length = self.working_length,
			sampleUnits = lal.Unit("strain")
		)
		self.fworkspace = lal.CreateCOMPLEX16FrequencySeries(
			name = "template",
			epoch = LIGOTimeGPS(0),
			f0 = 0.0,
			deltaF = 1.0 / self.working_duration,
			length = self.working_length // 2 + 1,
			sampleUnits = lal.Unit("strain s")
		)

		if FIR_WHITENER:
			self.kernel_fseries = create_FIR_whitener_kernel(self.working_length, self.working_duration, self.sample_rate_max, self.psd)

		# Calculate the maximum ring down time or maximum shift time
		if approximant in templates.gstlal_IMR_approximants:
			self.max_ringtime = max([chirptime.ringtime(row.mass1*lal.MSUN_SI + row.mass2*lal.MSUN_SI, chirptime.overestimate_j_from_chi(max(row.spin1z, row.spin2z))) for row in self.template_table])
		else:
			if self.sample_rate_max > 2. * self.fhigh:
			# Calculate the maximum time we need to shift the early warning
			# waveforms forward by, calculated by the 3.5 approximation from
			# fhigh to ISCO.
				self.max_shift_time = max([spawaveform.chirptime(row.mass1, row.mass2, 7, fhigh, 0., spawaveform.computechi(row.mass1, row.mass2, row.spin1z, row.spin2z)) for row in self.template_table])

			#
			# Generate each template, downsampling as we go to save memory
			# generate "cosine" component of frequency-domain template.
			# waveform is generated for a canonical distance of 1 Mpc.
			#

	def make_whitened_template(self, template_table_row):
		# FIXME: This is won't work
		#assert template_table_row in self.template_table, "The input Sngl_Inspiral:Table is not found in the workspace."

		# Create template
		fseries = generate_template(template_table_row, self.approximant, self.sample_rate_max, self.working_duration, self.f_low, self.fhigh, fwdplan = self.fwdplan, fworkspace = self.fworkspace)

		if FIR_WHITENER:
			#
			# Compute a product of freq series of the whitening kernel and the template (convolution in time domain) then add quadrature phase(Leo)
			#
			assert (len(self.kernel_fseries.data.data) // 2 + 1) == len(fseries.data.data), "the size of whitening kernel freq series does not match with a given format of COMPLEX16FrequencySeries."
			fseries.data.data *= self.kernel_fseries.data.data[len(self.kernel_fseries.data.data) // 2 - 1:]
			fseries = templates.QuadraturePhase.add_quadrature_phase(fseries, self.working_length)
		else:
			#
			# whiten and add quadrature phase ("sine" component)
			#

			if self.psd is not None:
				lal.WhitenCOMPLEX16FrequencySeries(fseries, self.psd)
				fseries = templates.QuadraturePhase.add_quadrature_phase(fseries, self.working_length)

		#
		# compute time-domain autocorrelation function
		#

		if self.autocorrelation_length is not None:
			autocorrelation = templates.normalized_autocorrelation(fseries, self.revplan).data.data
		else:
			autocorrelation = None

		#
		# transform template to time domain
		#

		lal.COMPLEX16FreqTimeFFT(self.tseries, fseries, self.revplan)

		data = self.tseries.data.data
		epoch_time = fseries.epoch.gpsSeconds + fseries.epoch.gpsNanoSeconds*1.e-9

		#
		# extract the portion to be used for filtering
		#

		#
		# condition the template if necessary (e.g. line up IMR
		# waveforms by peak amplitude)
		#

		if self.approximant in templates.gstlal_IMR_approximants:
			data, target_index = condition_imr_template(self.approximant, data, epoch_time, self.sample_rate_max, self.max_ringtime)
			# record the new end times for the waveforms (since we performed the shifts)
			template_table_row.end = LIGOTimeGPS(float(target_index-(len(data) - 1.))/self.sample_rate_max)
		else:
			if self.sample_rate_max > self.fhigh*2.:
				data, target_index = condition_ear_warn_template(self.approximant, data, epoch_time, self.sample_rate_max, self.max_shift_time)
				data *= tukeywindow(data, samps = 32)
				# record the new end times for the waveforms (since we performed the shifts)
				template_table_row.end = LIGOTimeGPS(float(target_index)/self.sample_rate_max)
			else:
				data *= tukeywindow(data, samps = 32)

		data = data[-self.length_max:]

		#
		# normalize so that inner product of template with itself
		# is 2
		#

		norm = abs(numpy.dot(data, numpy.conj(data)))
		data *= cmath.sqrt(2 / norm)

		#
		# sigmasq = 2 \sum_{k=0}^{N-1} |\tilde{h}_{k}|^2 / S_{k} \Delta f
		#
		# XLALWhitenCOMPLEX16FrequencySeries() computed
		#
		# \tilde{h}'_{k} = \sqrt{2 \Delta f} \tilde{h}_{k} / \sqrt{S_{k}}
		#
		# and XLALCOMPLEX16FreqTimeFFT() computed
		#
		# h'_{j} = \Delta f \sum_{k=0}^{N-1} exp(2\pi i j k / N) \tilde{h}'_{k}
		#
		# therefore, "norm" is
		#
		# \sum_{j} |h'_{j}|^{2} = (\Delta f)^{2} \sum_{j} \sum_{k=0}^{N-1} \sum_{k'=0}^{N-1} exp(2\pi i j (k-k') / N) \tilde{h}'_{k} \tilde{h}'^{*}_{k'}
		#                       = (\Delta f)^{2} \sum_{k=0}^{N-1} \sum_{k'=0}^{N-1} \tilde{h}'_{k} \tilde{h}'^{*}_{k'} \sum_{j} exp(2\pi i j (k-k') / N)
		#                       = (\Delta f)^{2} N \sum_{k=0}^{N-1} |\tilde{h}'_{k}|^{2}
		#                       = (\Delta f)^{2} N 2 \Delta f \sum_{k=0}^{N-1} |\tilde{h}_{k}|^{2} / S_{k}
		#                       = (\Delta f)^{2} N sigmasq
		#
		# and \Delta f = 1 / (N \Delta t), so "norm" is
		#
		# \sum_{j} |h'_{j}|^{2} = 1 / (N \Delta t^2) sigmasq
		#
		# therefore
		#
		# sigmasq = norm * N * (\Delta t)^2
		#

		sigmasq = norm * len(data) / self.sample_rate_max**2.

		return data, autocorrelation, sigmasq


#
# =======================================================================
#
#				parse data
#
# =======================================================================
#

xmldoc = ligolw_utils.load_filename("coinc_GW170817.xml", contenthandler=ContentHandler, verbose=True)	# change the file name ###

sngl_inspiral_table = lsctables.SnglInspiralTable.get_table(xmldoc)
DetectorName = tuple(sngl_inspiral_table.getColumnByName("ifo"))
n_ifo = len(DetectorName)

data_tree = np.array([xmldoc.getElementsByTagName(ligolw.LIGO_LW.tagName)[index] for index in range(n_ifo)])
	# data_tree[index_of_detector].array[time,real,complex]
cSNR = np.array([lalseries.parse_COMPLEX8TimeSeries(data_tree[index]) for index in range(n_ifo)])
	# correspond the order of DetectorName to the order of cSNR




"""	# HIDE FOR INJECTION
###########################################
#		dammy data injection
###########################################
# Hide all "HIDE"
GW_theta = 6*np.pi / 12		# set direction of injection
#GW_theta = np.deg2rad(87.66710548408382 +90)		# set direction of injection
#GW_theta = np.deg2rad(60)		# set direction of injection
#GW_theta = np.deg2rad(98.38553865)	# set direction of injection
#GW_theta = 3.0993959216659293	# on ring
GW_phi = 12*np.pi / 12		# set direction of injection
#GW_phi = np.deg2rad(-74.7714174257378 +90)		# set direction of injection
#GW_phi = np.deg2rad(310.78125)		# set direction of injection
#GW_phi = np.deg2rad(19.6875)		# set direction of injection
#GW_phi = -1.5707963267948966	# on ring

Detector = np.array([lalsimulation.DetectorPrefixToLALDetector(str(name)) for name in DetectorName])

# rewrite initial time
GWdirection = np.array([np.sin(GW_theta)*np.cos(GW_phi), np.sin(GW_theta)*np.sin(GW_phi), np.cos(GW_theta)])
initial_time = 0*np.array([cSNR[index].epoch for index in range(n_ifo)])	# reset
initial_time = np.array([np.dot(-GWdirection, Detector[i].location)/C_SI for i in range(n_ifo)])

T_ = len(cSNR[0].data.data)

toa = T_/2 - round(min(initial_time)/cSNR[0].deltaT)
#toa = round(T_/2.0 + (sum(initial_time) + np.dot(GWdirection, sum([Detector[i].location for i in range(n_ifo)]))/C_SI)/3/cSNR[0].deltaT)

#diff = GreenwichMeanSiderealTime(toa * cSNR[0].deltaT) % (2*np.pi)
diff = 0

F = np.array([ComputeDetAMResponse(Detector[index].response, GW_phi, np.pi/2-GW_theta, 0, diff) for index in range(n_ifo)])	# argument is radians; Set direection

#def h_form(t):
#	return np.exp(-1000*(t - cSNR[0].deltaT * T_/2)**2 + 1j * t*1000) * t*1000

#h = np.array([h_form(index_t * cSNR[0].deltaT) for index_t in range(T_)])	# virtual signal
h_r = np.random.randn(T_)/50
#h_i = np.random.randn(T_)
h_i = h_r

#'''
h_r *= np.array([(np.exp(-(i - T_/2)**2)/np.sqrt(np.pi))*50 for i in range(T_)])
h_i *= np.array([(np.exp(-(i - T_/2)**2)/np.sqrt(np.pi))*50 for i in range(T_)])
#'''

'''	# FOR NO Projection
for index in range(n_ifo):
	cSNR[index].data.data = 0 * cSNR[index].data.data	# reset
	cSNR[index].data.data += h_r
#'''

#'''	# FOR Projection
for index in range(n_ifo):
	cSNR[index].data.data *= 0	# reset
	cSNR[index].data.data += F[index][0]*h_r + 1j*F[index][1]*h_i
#'''

'''	# with noise
for index in range(n_ifo):
	cSNR[index].data.data += np.random.randn(T_)*1 + 1j*np.random.randn(T_)*1
	cSNR[index].data.data /= 2*np.sqrt(2)
#'''
cSNR[0].data.data += (np.random.rand(T_)-0.5)*2 + 1j*(np.random.rand(T_)-0.5)*2



#"""


"""
# For correction
#
PhaseGap = [cmath.phase(cSNR[index].data.data[np.argmax(abs(cSNR[index].data.data))]) for index in range(n_ifo)]	# FOR hand
#medianPSD = [np.median(abs(cSNR[index].data.data)) for index in range(n_ifo)]
medianPSD = np.array([1, 1, 1])
#"""


"""	# Switch all sentense for whitening
# whitening
#

xmldoc = ligolw_utils.load_filename("psd.xml", contenthandler=lalseries.PSDContentHandler)
referencexmldoc = ligolw_utils.load_filename("H1L1V1_O2REFERENCE_psd.xml", contenthandler=lalseries.PSDContentHandler)
for i in range(n_ifo):
	# read PSD
	psd = lalseries.read_psd_xmldoc(xmldoc)[DetectorName[i]]
	referencePSD = lalseries.read_psd_xmldoc(referencexmldoc)[DetectorName[i]]

	# generate template
	hplus, hcross = generate_template(sngl_inspiral_table[i], "TaylorF2", 1/cSNR[0].deltaT, 2*len(psd.data.data[1:])*cSNR[0].deltaT, psd.deltaF, round(psd.f0+psd.data.length*psd.deltaF))	# *2 is due to Nyquist frequency
	
	# normalization
	#referencePSD_adjust = np.array([sum(referencePSD.data.data[round(i*psd.deltaF/referencePSD.deltaF): round((i+1)*psd.deltaF/referencePSD.deltaF)-1]) for i in range(psd.data.length)])
	#hplus.data.data /= sum(abs(hplus.data.data)**2 / referencePSD_adjust)
	#hcross.data.data /= sum(abs(hcross.data.data)**2 / referencePSD_adjust)

	# variance of SNR
	df = 1 / (cSNR[0].data.length * cSNR[0].deltaT)
	S_rho = (abs(hplus.data.data)**2 + abs(hcross.data.data)**2) / psd.data.data
	SNR_variance = np.array([sum(S_rho[round(i*df/psd.deltaF): round((i+1)*df/psd.deltaF)-1]) for i in range(cSNR[0].data.length //2)]) / df	# /2 is due to Nyquist frequency
# TODO: Think necessity of S_rho[i] += S_rho[i + Nyquist] <- S_rho += S_rho[::-1]

	# put window
	w = np.array([np.sin(j * np.pi / (cSNR[i].data.length-1))**2 for j in range(cSNR[i].data.length)])
	cSNR[i].data.data *= w / sum(w)

	# whitening
	Fourier = np.fft.fft(cSNR[i].data.data)
	Fourier[:cSNR[i].data.length // 2] /= np.sqrt(SNR_variance)
	#Fourier[cSNR[i].data.length // 2 + 1:] = 0
	cSNR[i].data.data = np.fft.ifft(Fourier)
#"""


"""
# high/low-pass filter
#
df = 1 / (len(cSNR[0].data.data) * cSNR[0].deltaT)
for i in range(n_ifo):
	Fourier = np.fft.fft(cSNR[i].data.data)
	Fourier[:round(100/df)] = 0
	Fourier[round(300/df):] = 0
	cSNR[i].data.data = np.fft.ifft(Fourier)
#"""


#
# =======================================================================
#
#			zero padding data
#
# =======================================================================
#

#
# set initial time of data
#
initial_time = np.array([float(cSNR[index].epoch) for index in range(n_ifo)])	# HIDE FOR INJECTION

far_IFO = np.argmax(initial_time)
near_IFO = np.argmin(initial_time)
for i in range(n_ifo):				# TODO:switch up to the number of IFO but this switch is not used in Y_lm
	if i!=far_IFO and i!=near_IFO:
		medium_IFO = i

gap = round((initial_time[far_IFO] - initial_time[near_IFO]) / cSNR[0].deltaT)	# always positive, deltaT is same
gap_r = round((initial_time[far_IFO] - initial_time[medium_IFO]) / cSNR[0].deltaT)
gap_l = round((initial_time[medium_IFO] - initial_time[near_IFO]) / cSNR[0].deltaT)

ZeroPad_cSNR = [np.array([]) for x in range(n_ifo)]
ZeroPad_cSNR[far_IFO] = np.insert(cSNR[far_IFO].data.data, 0, np.zeros(gap))
ZeroPad_cSNR[near_IFO] = np.insert(cSNR[near_IFO].data.data, len(cSNR[near_IFO].data.data), np.zeros(gap))
ZeroPad_cSNR[medium_IFO] = np.insert(cSNR[medium_IFO].data.data, 0, np.zeros(gap_l))
ZeroPad_cSNR[medium_IFO] = np.insert(ZeroPad_cSNR[medium_IFO], len(ZeroPad_cSNR[medium_IFO]), np.zeros(gap_r))

#
# zero padding to calculate correlator
#
T = len(ZeroPad_cSNR[0])	# dulation

for index in range(n_ifo):
	ZeroPad_cSNR[index] = np.insert(ZeroPad_cSNR[index], 0, np.zeros(round(T/2)))
	ZeroPad_cSNR[index] = np.insert(ZeroPad_cSNR[index], len(ZeroPad_cSNR[index]), np.zeros(round(T/2)))

ZeroPad_cSNR = np.array(ZeroPad_cSNR)


#
# =======================================================================
#
#			calculate correlator
#
# =======================================================================
#

NSIDE = 64			# resolution
npix = hp.nside2npix(NSIDE)	# npix = 12 * NSIDE**2; number of pixel

correlator = 1j*np.zeros(npix)
#correlator = np.ones(npix) + 1j*np.zeros(npix)	# For test of maltiplication
Detector = np.array([lalsimulation.DetectorPrefixToLALDetector(str(name)) for name in DetectorName])

#diff = GreenwichMeanSiderealTime(cSNR[near_IFO].epoch + cSNR[near_IFO].deltaT * (round(T/2) -9)) % (2*np.pi)	# return radian; cSNR[near_IFO].deltaT * round(T/2) is center of data. You have to change up to t; HIDE FOR INJECTION	##
diff = GreenwichMeanSiderealTime(cSNR[near_IFO].epoch + cSNR[near_IFO].deltaT * (round(T/2) -30)) % (2*np.pi)	# GW170817; return radian; cSNR[near_IFO].deltaT * round(T/2) is center of data. You have to change up to t; HIDE FOR INJECTION	##

test = 1j*np.zeros((6,npix))	# delete this after mode decomposition test
duration = 87

for idx in range(npix):
	theta, phi = hp.pix2ang(NSIDE, idx)	# NOTE: return radians
	'''					# For high resolution
	if (theta>5*np.pi/6 or theta<4*np.pi/6) or (phi <np.pi/6 or phi>2*np.pi/6):
		continue
	#'''
	phi -= diff	# Using phi'=phi-diff to calculate but ploting with phi(=RA). Therefore consistent.
	
	direction = np.array([np.sin(theta)*np.cos(phi), np.sin(theta)*np.sin(phi), np.cos(theta)])

	F = np.array([ComputeDetAMResponse(Detector[index].response, phi-diff, np.pi/2-theta, 0, diff) for index in range(n_ifo)])	# NOTE: argument is radians; 
	Projection = np.matmul(np.matmul(F, np.linalg.inv(np.matmul(F.T, F))), F.T)

	i = 0	# delete this after mode decomposition
	#
	# off-diagonal part
	#
	for IFO in itertools.combinations(range(n_ifo), 2):
		TimeDelay_0 = round(np.dot(Detector[IFO[0]].location, direction) / (C_SI * cSNR[0].deltaT))
		TimeDelay_1 = round(np.dot(Detector[IFO[1]].location, direction) / (C_SI * cSNR[0].deltaT))

		#corr_baseline = np.dot( ZeroPad_cSNR[IFO[0]][T-9-TimeDelay_0-round(duration/2):T-9-TimeDelay_0+round(duration/2)], np.conjugate(ZeroPad_cSNR[IFO[1]][T-9-TimeDelay_1-round(duration/2):T-9-TimeDelay_1+round(duration/2)]) )	# for GW170814
		corr_baseline = np.dot( ZeroPad_cSNR[IFO[0]][T-30-TimeDelay_0-round(duration/2):T-30-TimeDelay_0+round(duration/2)], np.conjugate(ZeroPad_cSNR[IFO[1]][T-30-TimeDelay_1-round(duration/2):T-30-TimeDelay_1+round(duration/2)]) )	# for GW170817
		#corr_baseline = np.dot( ZeroPad_cSNR[IFO[0]][round(T/2)+toa-TimeDelay_0-round(duration/2):round(T/2)+toa-TimeDelay_0+round(duration/2)], np.conjugate(ZeroPad_cSNR[IFO[1]][round(T/2)+toa-TimeDelay_1-round(duration/2):round(T/2)+toa-TimeDelay_1+round(duration/2)]) )	# NO HIDE FOR INJECTION

		#corr_baseline *= np.exp(-1j*(PhaseGap[IFO[0]]-PhaseGap[IFO[1]]))	# FOR hand
		#correlator[idx] += -2 * Projection[IFO[0], IFO[1]] * corr_baseline / np.sqrt(medianPSD[IFO[0]] * medianPSD[IFO[1]])	# FOR hand

		correlator[idx] += 2 * Projection[IFO[0], IFO[1]] * corr_baseline	# factor 2 is from \\alpha > \\beta
		#correlator[idx] *= corr_baseline
		test[i][idx] = 2 * Projection[IFO[0]][IFO[1]] * corr_baseline	# delete this after mode decomposition
		i += 1	# delete this after mode decomposition

	"""
	# diagonal part
	#
	for IFO in range(n_ifo):
		TimeDelay = round(np.dot(Detector[IFO].location, direction) / (C_SI * cSNR[0].deltaT))
		#corr_baseline = np.dot( ZeroPad_cSNR[IFO][T-9-TimeDelay-round(duration/2):T-9-TimeDelay+round(duration/2)], np.conjugate(ZeroPad_cSNR[IFO][T-9-TimeDelay-round(duration/2):T-9-TimeDelay+round(duration/2)]) )	# for GW170814
		corr_baseline = np.dot( ZeroPad_cSNR[IFO][T-30-TimeDelay-round(duration/2):T-30-TimeDelay+round(duration/2)], np.conjugate(ZeroPad_cSNR[IFO][T-30-TimeDelay-round(duration/2):T-30-TimeDelay+round(duration/2)]) )	# for GW170817
		#corr_baseline = np.dot( ZeroPad_cSNR[IFO][round(T/2)+toa-TimeDelay-round(duration/2):round(T/2)+toa-TimeDelay+round(duration/2)], np.conjugate(ZeroPad_cSNR[IFO][round(T/2)+toa-TimeDelay-round(duration/2):round(T/2)+toa-TimeDelay+round(duration/2)]) )	# NO HIDE FOR INJECTION

		#correlator[idx] += (Projection[IFO, IFO] - 1) * corr_baseline
		correlator[idx] += Projection[IFO, IFO] * corr_baseline
		test[i][idx] = Projection[IFO][IFO] * corr_baseline	# delete this after mode decomposition
		i += 1	# delete this after mode decomposition
	#"""

print( np.rad2deg(hp.pix2ang(NSIDE, np.argmax(abs(correlator)))) )

# prior
prior = np.zeros(npix)
for idx in range(npix):
	theta, phi = hp.pix2ang(NSIDE, idx)	# return radians
	prior[idx] = np.sin(theta)

correlator = correlator.real
logLikelihood = correlator / duration		# for non-whitening
#logLikelihood = correlator * cSNR[0].deltaT	# for whitening
logposterior = logLikelihood + np.log(prior)
#logLikelihood -= logsumexp(logLikelihood)	# normalize
logposterior -= logsumexp(logposterior)		# normalize
Likelihood = np.exp(logLikelihood)
posterior = np.exp(logposterior)

print( np.rad2deg(hp.pix2ang(NSIDE, np.argmax(Likelihood))) )
print( np.rad2deg(hp.pix2ang(NSIDE, np.argmax(posterior))) )


#
# =======================================================================
#
#				plot into map
#
# =======================================================================
#

usedIFO = ""
for name in DetectorName:
	usedIFO += name

#"""
pp = PdfPages("map.pdf")

fig = plt.figure()
plot = fig.add_subplot(1,1,1)
#hp.mollview(correlator, title="positoin of GW170814 %s" % usedIFO, unit="log Likelihood", rot=(180,0,0), hold=True)
hp.mollview(logLikelihood, title="positoin of GW170817 %s" % usedIFO, unit="log(Likelihood)", rot=(180,0,0), hold=True)
hp.graticule()
plt.savefig(pp, format="pdf")
fig.clf()

fig = plt.figure()
plot = fig.add_subplot(1,1,1)
#hp.mollview(logposterior, title="positoin of GW170817 %s" % usedIFO, unit="log(posterior)", rot=(180,0,0), hold=True)
hp.mollview(posterior, title="positoin of GW170814 %s" % usedIFO, unit="posterior", rot=(180,0,0), hold=True)
#hp.mollview(Likelihood, title="positoin of GW170814 %s" % usedIFO, unit="Likelihood ratio", rot=(180,0,0), hold=True)
hp.graticule()
plt.savefig(pp, format="pdf")
fig.clf()

cmap = cm.BuPu
cmap.set_under("w")
fig = plt.figure()
plot = fig.add_subplot(1,1,1)
hp.mollview(Likelihood, title="position of GW170817", unit="Likelihood ratio", rot=(180,0,0), hold=True, cmap=cmap)
hp.graticule()
plt.savefig(pp, format="pdf")
fig.clf()

fig = plt.figure()
plot = fig.add_subplot(1,1,1)
#hp.cartview(Likelihood, lonra=[np.rad2deg(GW_phi-np.pi/12),np.rad2deg(GW_phi+np.pi/12)], latra=[np.rad2deg(np.pi/2-GW_theta-np.pi/12),np.rad2deg(np.pi/2-GW_theta+np.pi/12)], title="Zoom: position of GW170814 %s" % usedIFO, unit="posterior", hold=True)	# NO HIDE FOR INJECTION
#hp.cartview(posterior, lonra=[np.rad2deg(GW_phi-np.pi/12),np.rad2deg(GW_phi+np.pi/12)], latra=[np.rad2deg(np.pi/2-GW_theta-np.pi/12),np.rad2deg(np.pi/2-GW_theta+np.pi/12)], title="Zoom: position of GW170814 %s" % usedIFO, unit="posterior", hold=True)	# NO HIDE FOR INJECTION
hp.cartview(Likelihood, lonra=[185,215], latra=[-45,-15], title="Zoom: position of GW170817 %s" % usedIFO, unit="log Likelihood ratio", hold=True)	# HIDE FOR INJECTION
#hp.cartview(logposterior, lonra=[10,50], latra=[-64,-30], title="Zoom: position of GW170814 %s" % usedIFO, unit="log(posterior)", hold=True)	# HIDE FOR INJECTION
#hp.cartview(posterior, lonra=[30,60], latra=[-60,-30], title="Zoom: position of GW170814 %s" % usedIFO, unit="posterior", hold=True)	# HIDE FOR INJECTION
#hp.graticule()
plt.savefig(pp, format="pdf")
fig.clf()

pp.close()
#"""
"""
hp.cartview(Likelihood, lonra=[30,60], latra=[-60,-30], title="position of GW170814 %s" % usedIFO, unit="Likelihood ratio")
hp.graticule()
#"""
"""
hp.mollview(correlator, title="position of GW170814 %s" % usedIFO, unit="correlation magnitude", rot=(180,0,0))
hp.graticule()
#"""

#plt.savefig("map.eps")
#hp.write_map("GWmap.fits", posterior)

# delete this after mode decomposition
test = test.real
test /= duration			# logLikelihood
#test += np.log(prior) / len(test)	# logposterior
#test -= logsumexp(sum([test[i] for i in range(len(test))]) / len(test))	#normalization

plt.clf()
fig, ax = plt.subplots(3,2)
j=0
for IFO in itertools.combinations(range(3),2):
	plt.axes(ax[j,0])
	hp.mollview(test[j], title="P_%s%s" % (str(DetectorName[IFO[0]]),str(DetectorName[IFO[1]])), rot=(180,0,0), hold=True)
	hp.graticule()
	j += 1

for j in range(3):
	plt.axes(ax[j,1])
	hp.mollview(test[j+3], title="P_%s%s" % (str(DetectorName[j]),str(DetectorName[j])), rot=(180,0,0), hold=True)
	hp.graticule()

plt.savefig("contribution_from_modes.eps")


end = time.time()
print("")
print( "integration duration:", len(cSNR[0].data.data)*cSNR[0].deltaT, "s" )
print( "processing time: ", end-start, "s")
print("")


"""
hp.cartview(correlator, lonra=[30,60], latra=[-60,-30], title="position of GW170814 %s" % usedIFO, unit="Likelihood ratio")
hp.graticule()

a = hp.read_map("bayestar.fits")

np.rad2deg(hp.pix2ang(NSIDE, np.argmax(posterior)))
ang_gap=np.rad2deg(hp.pix2ang(1024, np.argmax(a)))-np.rad2deg(hp.pix2ang(NSIDE, np.argmax(posterior)))
np.sqrt(np.matmul(ang_gap, ang_gap))

t = np.arange(len(ZeroPad_cSNR[0]))
for i in range(3):
    plt.plot(t,ZeroPad_cSNR[i].real)

plt.show()
"""


"""
>>> np.rad2deg(np.arccos(1-cSNR[0].deltaT * C_SI/1000/6400))
12.277905591221876

>>> np.rad2deg(hp.pix2ang(1024, np.argmax(a)))
array([135.48794615,  41.06842105])	# GW170814
array([107.8568951 , 194.30419922])	# GW170817; deg, theta, phi

The gap of peak between bayestar and mine using only off-diagonal:
5.522243071109678 deg for GW170814
18.363573386855574 deg for GW 170817
22.995601296560306 deg for GW170817 with high/low-pass filter

The gap of peak between bayestar and map_GW_diagonalshift_hand_NSIDE1024.pdf is
	2.1278662299197806 deg


"""
