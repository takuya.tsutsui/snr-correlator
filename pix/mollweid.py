import numpy as np
import cmath
import matplotlib
#matplotlib.use("Agg")
from matplotlib import pyplot as plt
from matplotlib import cm as cm
from matplotlib.backends.backend_pdf import PdfPages
import healpy as hp
import time
import itertools
from scipy.misc import logsumexp

from glue.ligolw import ligolw
from glue.ligolw import lsctables
from glue.ligolw import utils as ligolw_utils
from lal import series as lalseries
from lal import GreenwichMeanSiderealTime, ComputeDetAMResponse, C_SI
import lalsimulation



@lsctables.use_in
class ContentHandler(lalseries.PSDContentHandler):
	pass
	

start = time.time()


#
# =======================================================================
#
#				parse data
#
# =======================================================================
#

xmldoc = ligolw_utils.load_filename("coinc_GW170817.xml", contenthandler=ContentHandler, verbose=True)	# change the file name ###

sngl_inspiral_table = lsctables.SnglInspiralTable.get_table(xmldoc)
DetectorName = tuple(sngl_inspiral_table.getColumnByName("ifo"))
n_ifo = len(DetectorName)

data_tree = np.array([xmldoc.getElementsByTagName(ligolw.LIGO_LW.tagName)[index] for index in range(n_ifo)])
	# data_tree[index_of_detector].array[time,real,complex]
cSNR = np.array([lalseries.parse_COMPLEX8TimeSeries(data_tree[index]) for index in range(n_ifo)])
	# correspond the order of DetectorName to the order of cSNR




"""	# HIDE FOR INJECTION
###########################################
#		dammy data injection
###########################################
# Hide all "HIDE"
GW_theta = 6*np.pi / 12		# set direction of injection
#GW_theta = np.deg2rad(87.66710548408382 +90)		# set direction of injection
#GW_theta = np.deg2rad(60)		# set direction of injection
#GW_theta = np.deg2rad(98.38553865)	# set direction of injection
#GW_theta = 3.0993959216659293	# on ring
GW_phi = 12*np.pi / 12		# set direction of injection
#GW_phi = np.deg2rad(-74.7714174257378 +90)		# set direction of injection
#GW_phi = np.deg2rad(310.78125)		# set direction of injection
#GW_phi = np.deg2rad(19.6875)		# set direction of injection
#GW_phi = -1.5707963267948966	# on ring

Detector = np.array([lalsimulation.DetectorPrefixToLALDetector(str(name)) for name in DetectorName])

# rewrite initial time
GWdirection = np.array([np.sin(GW_theta)*np.cos(GW_phi), np.sin(GW_theta)*np.sin(GW_phi), np.cos(GW_theta)])
initial_time = 0*np.array([cSNR[index].epoch for index in range(n_ifo)])	# reset
initial_time = np.array([np.dot(-GWdirection, Detector[i].location)/C_SI for i in range(n_ifo)])

T_ = len(cSNR[0].data.data)

toa = T_/2 - int(round(min(initial_time)/cSNR[0].deltaT))
#toa = int(round(T_/2.0 + (sum(initial_time) + np.dot(GWdirection, sum([Detector[i].location for i in range(n_ifo)]))/C_SI)/3/cSNR[0].deltaT))

#diff = GreenwichMeanSiderealTime(toa * cSNR[0].deltaT) % (2*np.pi)
diff = 0

F = np.array([ComputeDetAMResponse(Detector[index].response, GW_phi, np.pi/2-GW_theta, 0, diff) for index in range(n_ifo)])	# argument is radians; Set direection

#def h_form(t):
#	return np.exp(-1000*(t - cSNR[0].deltaT * T_/2)**2 + 1j * t*1000) * t*1000

#h = np.array([h_form(index_t * cSNR[0].deltaT) for index_t in range(T_)])	# virtual signal
h_r = np.random.randn(T_)/50
#h_i = np.random.randn(T_)
h_i = h_r

#'''
h_r *= np.array([(np.exp(-(i - T_/2)**2)/np.sqrt(np.pi))*50 for i in range(T_)])
h_i *= np.array([(np.exp(-(i - T_/2)**2)/np.sqrt(np.pi))*50 for i in range(T_)])
#'''

'''	# FOR NO Projection
for index in range(n_ifo):
	cSNR[index].data.data = 0 * cSNR[index].data.data	# reset
	cSNR[index].data.data += h_r
#'''

#'''	# FOR Projection
for index in range(n_ifo):
	cSNR[index].data.data *= 0	# reset
	cSNR[index].data.data += F[index][0]*h_r + 1j*F[index][1]*h_i
#'''

'''	# with noise
for index in range(n_ifo):
	cSNR[index].data.data += np.random.randn(T_)*1 + 1j*np.random.randn(T_)*1
	cSNR[index].data.data /= 2*np.sqrt(2)
#'''
cSNR[0].data.data += (np.random.rand(T_)-0.5)*2 + 1j*(np.random.rand(T_)-0.5)*2



#"""


#
# For correction
#
PhaseGap = [cmath.phase(cSNR[index].data.data[np.argmax(abs(cSNR[index].data.data))]) for index in range(n_ifo)]	# FOR hand
#medianPSD = [np.median(abs(cSNR[index].data.data)) for index in range(n_ifo)]
medianPSD = np.array([1, 1, 1])


#
# =======================================================================
#
#			zero padding data
#
# =======================================================================
#

#
# set initial time of data
#
initial_time = np.array([cSNR[index].epoch for index in range(n_ifo)])	# HIDE FOR INJECTION

far_IFO = np.argmax(initial_time)
near_IFO = np.argmin(initial_time)
for i in range(n_ifo):				# TODO:switch up to the number of IFO but this switch is not used in Y_lm
	if i!=far_IFO and i!=near_IFO:
		medium_IFO = i

gap = int(round((initial_time[far_IFO] - initial_time[near_IFO]) / cSNR[0].deltaT))	# always positive, deltaT is same
gap_r = int(round((initial_time[far_IFO] - initial_time[medium_IFO]) / cSNR[0].deltaT))
gap_l = int(round((initial_time[medium_IFO] - initial_time[near_IFO]) / cSNR[0].deltaT))

ZeroPad_cSNR = [np.array([]) for x in range(n_ifo)]
ZeroPad_cSNR[far_IFO] = np.insert(cSNR[far_IFO].data.data, 0, np.zeros(gap))
ZeroPad_cSNR[near_IFO] = np.insert(cSNR[near_IFO].data.data, len(cSNR[near_IFO].data.data), np.zeros(gap))
ZeroPad_cSNR[medium_IFO] = np.insert(cSNR[medium_IFO].data.data, 0, np.zeros(gap_l))
ZeroPad_cSNR[medium_IFO] = np.insert(ZeroPad_cSNR[medium_IFO], len(ZeroPad_cSNR[medium_IFO]), np.zeros(gap_r))

#
# zero padding to calculate correlator
#
T = len(ZeroPad_cSNR[0])	# dulation; T/2 is integer even thogh T is odd number

for index in range(n_ifo):
	ZeroPad_cSNR[index] = np.insert(ZeroPad_cSNR[index], 0, np.zeros(T/2))
	ZeroPad_cSNR[index] = np.insert(ZeroPad_cSNR[index], len(ZeroPad_cSNR[index]), np.zeros(T/2))

ZeroPad_cSNR = np.array(ZeroPad_cSNR)


#
# =======================================================================
#
#			calculate correlator
#
# =======================================================================
#

NSIDE = 32			# resolution
npix = hp.nside2npix(NSIDE)	# npix = 12 * NSIDE**2; number of pixel

correlator = 1j*np.zeros(npix)
#correlator = np.ones(npix) + 1j*np.zeros(npix)	# For test of maltiplication
Detector = np.array([lalsimulation.DetectorPrefixToLALDetector(str(name)) for name in DetectorName])

#diff = GreenwichMeanSiderealTime(cSNR[near_IFO].epoch + cSNR[near_IFO].deltaT * (T/2 -9)) % (2*np.pi)	# return radian; cSNR[near_IFO].deltaT * T/2 is center of data. You have to change up to t; HIDE FOR INJECTION	##
diff = GreenwichMeanSiderealTime(cSNR[near_IFO].epoch + cSNR[near_IFO].deltaT * (T/2 -30)) % (2*np.pi)	# GW170817; return radian; cSNR[near_IFO].deltaT * T/2 is center of data. You have to change up to t; HIDE FOR INJECTION	##

test = 1j*np.zeros((6,npix))	# delete this after mode decomposition test

for idx in range(npix):
	theta, phi = hp.pix2ang(NSIDE, idx)	# NOTE: return radians
	'''					# For high resolution
	if (theta>5*np.pi/6 or theta<4*np.pi/6) or (phi <np.pi/6 or phi>2*np.pi/6):
		continue
	#'''
	phi -= diff	# Using phi'=phi-diff to calculate but ploting with phi(=RA). Therefore consistent.
	
	direction = np.array([np.sin(theta)*np.cos(phi), np.sin(theta)*np.sin(phi), np.cos(theta)])

	F = np.array([ComputeDetAMResponse(Detector[index].response, phi-diff, np.pi/2-theta, 0, diff) for index in range(n_ifo)])	# NOTE: argument is radians; 
	Projection = np.matmul(np.matmul(F, np.linalg.inv(np.matmul(F.T, F))), F.T)

	i = 0				# delete this after mode decomposition
	#
	# off-diagonal part
	#
	for IFO in itertools.combinations(range(n_ifo), 2):
		TimeDelay_0 = int(round(np.dot(Detector[IFO[0]].location, direction) / (C_SI * cSNR[0].deltaT)))
		TimeDelay_1 = int(round(np.dot(Detector[IFO[1]].location, direction) / (C_SI * cSNR[0].deltaT)))

		corr_baseline = 0j
		for t_ in range(-T/20, T/20):
			#corr_baseline += ZeroPad_cSNR[IFO[0]][T-9 + t_ - TimeDelay_0] * np.conjugate(ZeroPad_cSNR[IFO[1]][T-9 + t_ - TimeDelay_1]) #* cSNR[0].deltaT	# T-10 is the time of arival; HIDE FOR INJECTION	##
			corr_baseline += ZeroPad_cSNR[IFO[0]][T-30 + t_ - TimeDelay_0] * np.conjugate(ZeroPad_cSNR[IFO[1]][T-30 + t_ - TimeDelay_1]) #* cSNR[0].deltaT	# T-10 is the time of arival; HIDE FOR INJECTION	##
			#corr_baseline += ZeroPad_cSNR[IFO[0]][T/2 + toa + t_ - TimeDelay_0] * np.conjugate(ZeroPad_cSNR[IFO[1]][T/2 + toa + t_ - TimeDelay_1]) #* cSNR[0].deltaT	# NO HIDE FOR INJECTION


		#corr_baseline *= np.exp(-1j*(PhaseGap[IFO[0]]-PhaseGap[IFO[1]]))	# FOR hand
		#correlator[idx] += -Projection[IFO[0], IFO[1]] * corr_baseline / np.sqrt(medianPSD[IFO[0]] * medianPSD[IFO[1]])	# FOR hand

		correlator[idx] += Projection[IFO[0], IFO[1]] * corr_baseline
		#correlator[idx] *= corr_baseline
		test[i][idx] = Projection[IFO[0]][IFO[1]] * corr_baseline	# delete this after mode decomposition
		i += 1	# delete this after mode decomposition

	#"""
	# diagonal part
	#
	for IFO in range(n_ifo):
		TimeDelay = int(round(np.dot(Detector[IFO].location, direction) / (C_SI * cSNR[0].deltaT)))
		corr_baseline = 0j
		for t_ in range(-T/20, T/20):
			#corr_baseline += ZeroPad_cSNR[IFO][T-9 + t_] * np.conjugate(ZeroPad_cSNR[IFO][T-9 + t_]) #* cSNR[0].deltaT	# HIDE FOR INJECTION ##/10
			corr_baseline += ZeroPad_cSNR[IFO][T-30 + t_] * np.conjugate(ZeroPad_cSNR[IFO][T-30 + t_]) #* cSNR[0].deltaT	# HIDE FOR INJECTION ##/10
			#corr_baseline += ZeroPad_cSNR[IFO][T/2 + toa + t_ - TimeDelay] * np.conjugate(ZeroPad_cSNR[IFO][T/2 +toa + t_ - TimeDelay]) #* cSNR[0].deltaT	# NO HIDE FOR INJECTION

		#correlator[idx] += (Projection[IFO, IFO] - 1) * corr_baseline / 2
		correlator[idx] += Projection[IFO, IFO] * corr_baseline / 2
		test[i][idx] = Projection[IFO][IFO] * corr_baseline /2 / abs(medianPSD[IFO])	# delete this after mode decomposition
		i += 1	# delete this after mode decomposition
	#"""

print np.rad2deg(hp.pix2ang(NSIDE, np.argmax(abs(correlator))))

# prior
prior = np.zeros(npix)
for idx in range(npix):
	theta, phi = hp.pix2ang(NSIDE, idx)	# return radians
	prior[idx] = np.sin(theta)

correlator = correlator.real
logLikelihood = correlator * T/10
logposterior = logLikelihood + np.log(prior)
logLikelihood -= logsumexp(logLikelihood)	# normalize
logposterior -= logsumexp(logposterior)		# normalize
Likelihood = np.exp(logLikelihood)
posterior = np.exp(logposterior)

print np.rad2deg(hp.pix2ang(NSIDE, np.argmax(abs(Likelihood))))
print np.rad2deg(hp.pix2ang(NSIDE, np.argmax(abs(posterior))))


#
# =======================================================================
#
#				plot into map
#
# =======================================================================
#

usedIFO = ""
for name in DetectorName:
	usedIFO += name

#"""
pp = PdfPages("map.pdf")

fig = plt.figure()
plot = fig.add_subplot(1,1,1)
#hp.mollview(correlator, title="positoin of GW170814 %s" % usedIFO, unit="log Likelihood", rot=(180,0,0), hold=True)
hp.mollview(logLikelihood, title="positoin of GW170814 %s" % usedIFO, unit="log(Likelihood)", rot=(180,0,0), hold=True)
hp.graticule()
plt.savefig(pp, format="pdf")
fig.clf()

fig = plt.figure()
plot = fig.add_subplot(1,1,1)
hp.mollview(logposterior, title="positoin of GW170814 %s" % usedIFO, unit="log(posterior)", rot=(180,0,0), hold=True)
#hp.mollview(posterior, title="positoin of GW170814 %s" % usedIFO, unit="posterior", rot=(180,0,0), hold=True)
#hp.mollview(Likelihood, title="positoin of GW170814 %s" % usedIFO, unit="Likelihood ratio", rot=(180,0,0), hold=True)
hp.graticule()
plt.savefig(pp, format="pdf")
fig.clf()

fig = plt.figure()
plot = fig.add_subplot(1,1,1)
hp.mollview(Likelihood, title="position of GW170814", unit="Likelihood ratio", rot=(180,0,0), hold=True)
hp.graticule()
plt.savefig(pp, format="pdf")
fig.clf()

fig = plt.figure()
plot = fig.add_subplot(1,1,1)
#hp.cartview(Likelihood, lonra=[np.rad2deg(GW_phi-np.pi/12),np.rad2deg(GW_phi+np.pi/12)], latra=[np.rad2deg(np.pi/2-GW_theta-np.pi/12),np.rad2deg(np.pi/2-GW_theta+np.pi/12)], title="Zoom: position of GW170814 %s" % usedIFO, unit="posterior", hold=True)	# NO HIDE FOR INJECTION
#hp.cartview(posterior, lonra=[np.rad2deg(GW_phi-np.pi/12),np.rad2deg(GW_phi+np.pi/12)], latra=[np.rad2deg(np.pi/2-GW_theta-np.pi/12),np.rad2deg(np.pi/2-GW_theta+np.pi/12)], title="Zoom: position of GW170814 %s" % usedIFO, unit="posterior", hold=True)	# NO HIDE FOR INJECTION
hp.cartview(Likelihood, lonra=[30,60], latra=[-60,-30], title="Zoom: position of GW170814 %s" % usedIFO, unit="Likelihood ratio", hold=True)	# HIDE FOR INJECTION
#hp.cartview(logposterior, lonra=[10,50], latra=[-64,-30], title="Zoom: position of GW170814 %s" % usedIFO, unit="log(posterior)", hold=True)	# HIDE FOR INJECTION
#hp.cartview(posterior, lonra=[30,60], latra=[-60,-30], title="Zoom: position of GW170814 %s" % usedIFO, unit="posterior", hold=True)	# HIDE FOR INJECTION
#hp.graticule()
plt.savefig(pp, format="pdf")
fig.clf()

pp.close()
#"""
"""
hp.cartview(Likelihood, lonra=[30,60], latra=[-60,-30], title="position of GW170814 %s" % usedIFO, unit="Likelihood ratio")
hp.graticule()
#"""
"""
hp.mollview(correlator, title="position of GW170814 %s" % usedIFO, unit="correlation magnitude", rot=(180,0,0))
hp.graticule()
#"""

#plt.savefig("map.eps")
#hp.write_map("GWmap.fits", posterior)

# delete this after mode decomposition
test = test.real
test *= T/10				# logLikelihood
test += np.log(prior) / len(test)	# logposterior
test -= logsumexp(sum([test[i] for i in range(len(test))]) / len(test))	#normalization

plt.clf()
fig, ax = plt.subplots(3,2)
j=0
for IFO in itertools.combinations(range(3),2):
	plt.axes(ax[j,0])
	hp.mollview(test[j], title="P_%s%s" % (str(DetectorName[IFO[0]]),str(DetectorName[IFO[1]])), rot=(180,0,0), hold=True)
	hp.graticule()
	j += 1

for j in range(3):
	plt.axes(ax[j,1])
	hp.mollview(test[j+3], title="P_%s%s" % (str(DetectorName[j]),str(DetectorName[j])), rot=(180,0,0), hold=True)
	hp.graticule()

plt.savefig("contribution_from_modes.eps")


end = time.time()
print
print "integration duration:" + str(len(cSNR[0].data.data)*cSNR[0].deltaT) + " s"
print "processing time: " + str(end-start) + " s"
print


"""
hp.cartview(correlator, lonra=[30,60], latra=[-60,-30], title="position of GW170814 %s" % usedIFO, unit="Likelihood ratio")
hp.graticule()

a = hp.read_map("bayestar.fits")

np.rad2deg(hp.pix2ang(NSIDE, np.argmax(posterior)))
ang_gap=np.rad2deg(hp.pix2ang(1024, np.argmax(a)))-np.rad2deg(hp.pix2ang(NSIDE, np.argmax(posterior)))
np.sqrt(np.matmul(ang_gap, ang_gap))

t = np.arange(len(ZeroPad_cSNR[0]))
for i in range(3):
    plt.plot(t,ZeroPad_cSNR[i].real)

plt.show()
"""


"""
>>> np.rad2deg(np.arccos(1-cSNR[0].deltaT * C_SI/1000/6400))
12.277905591221876

>>> np.rad2deg(hp.pix2ang(1024, np.argmax(a)))
array([135.48794615,  41.06842105])

The gap of peak between bayestar and map_GW_diagonalshift_hand_NSIDE1024.pdf is
	2.1278662299197806 deg


"""
