\documentclass[%
 reprint,
%superscriptaddress,
%groupedaddress,
%unsortedaddress,
%runinaddress,
%frontmatterverbose, 
%preprint,
%preprintnumbers,
 nofootinbib, % footnotes are on bottom of the page or after references
 nobibnotes,
 amsmath,amssymb,
 aps,
%pra,
%prb,
%rmp,
%prstab,
%prstper,
%floatfix,
dvipdfmx,
]{revtex4-2}

\usepackage{graphicx}
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage[hypertexnames=false]{hyperref}% add hypertext capabilities
%\usepackage[mathlines]{lineno}% Enable numbering of text and display math
\usepackage{here}
\usepackage{enumerate}
\usepackage{bm}
\usepackage{mathrsfs}
\usepackage{comment}
\usepackage{acronym}
\usepackage{url}
\usepackage{siunitx}


%\linenumbers\relax % Commence numbering lines

%\usepackage[showframe,%Uncomment any one of the following lines to test 
%%scale=0.7, marginratio={1:1, 2:3}, ignoreall,% default settings
%%text={7in,10in},centering,
%%margin=1.5in,
%%total={6.5in,8.75in}, top=1.2in, left=0.9in, includefoot,
%%height=10in,a5paper,hmargin={3cm,0.8in},
%]{geometry}

\newcommand{\unit}[1]{\,\rm{#1}}
\newcommand{\eq}[2][]{
	\begin{align#1}
		#2
	\end{align#1}
}

\newcommand{\myfig}[4][width=\linewidth]{
    \begin{figure}[tb]
        \centering
            \includegraphics[#1]{#2}    % #2:画像の名前
        \caption{#3}                    % #3:キャプションの名前
        \label{#4} %ラベルをつけることにより参照できるようになります。
    \end{figure}
}

\newcommand{\diag}{{\rm diag}}
\newcommand{\diff}{\mathrm{d}}
\newcommand{\iu}{\mathrm{i}}
\newcommand{\e}{\mathrm{e}}
\newcommand{\sinc}{{\rm{sinc}}}
\newcommand{\cross}{\times}
\newcommand{\figref}[1]{Fig.~\ref{#1}}
\newcommand{\tabref}[1]{Table~\ref{#1}}
\newcommand{\secref}[1]{Sec.~\ref{#1}}
\newcommand{\appref}[1]{Appendix~\ref{#1}}
\newcommand{\chapref}[1]{Chapter~\ref{#1}}
\newcommand{\av}[1]{\left\langle #1 \right\rangle}
\newcommand{\hatilde}[1]{\hat{\tilde{#1}}}

\allowdisplaybreaks[4] % 数式を途中で改ページする．[1]-[4]までのレベルがある．

\input{acros}

\begin{acronym}
\input{acros}
\end{acronym} % colon is from this

\begin{document}

%\preprint{APS/123-QED}

\title{High Speed Source Localization in Searches for Gravitational Waves from Compact Object Collisions}% Force line breaks with \\
%\thanks{A footnote to the article title}%

\author{Takuya Tsutsui}
	%\altaffiliation[Also at ]{Physics Department, University of Tokyo.}%Lines break automatically or can be forced with \\
\author{Kipp Cannon}%
\author{Leo Tsukada}%
%\email{Second.Author@institution.edu}
%\affiliation{%
% Authors' institution and/or address\\
% This line break forced with \textbackslash\textbackslash
%}%

%\collaboration{MUSO Collaboration}%\noaffiliation

%\author{Kipp Cannon}
%\homepage{http://www.Second.institution.edu/~Charlie.Author}
%\affiliation{
% Second institution and/or address\\
% This line break forced% with \\
%}%
%\affiliation{
% Third institution, the second for Charlie Author
%}%
%\author{Delta Author}
%\affiliation{%
% Authors' institution and/or address\\
% This line break forced with \textbackslash\textbackslash
%}%

%\collaboration{CLEO Collaboration}%\noaffiliation

\date{\today}% It is always \today, today,
             %  but any date may be explicitly specified

\begin{abstract}
Multi-messenger astronomy is of great interest since the success of the electromagnetic follow up of the neutron star merger GW170817. However, the information that was learned from GW170817 was limited by the long delay in finding the optical transient. Even in the best-case scenario, the current gravitational-wave source localization method is not sufficient for some frequency bands. Therefore, one needs a more rapid localization method even if it is less accurate. Building upon an Excess power method, we describe a new localization method for compact object collisions that produces posterior probability maps in only a few hundred milliseconds. Some accuracy is lost, with the searched sky areas being approximately $10$ times larger. We imagine this new technique playing a role in a hierarchical scheme were fast early location estimates are iteratively improved upon as better analyses complete on longer time scales.


%\begin{description}
%\item[Usage]
%Secondary publications and information retrieval purposes.
%\item[Structure]
%You may use the \texttt{description} environment to structure your abstract;
%use the optional argument of the \verb+\item+ command to give the category of each item. 
%\end{description}
\end{abstract}

%\keywords{Suggested keywords}%Use showkeys class option if keyword
                              %display desired
\maketitle

%\tableofcontents

\section{Introduction}

In August 17 2017, the advanced LIGO~\cite{LIGO1, LIGO2} and the advanced Virgo~\cite{Virgo} observed a \ac{GW} from \ac{BNS} merger, dubbed as GW170817~\cite{GW170817_observation}. Then, many \ac{EM} telescopes followed it to find the \ac{EM} counterpart with multi-wavelength from radio wave to gamma ray~\cite{GW170817_multimessenger}. By these observations, \ac{BNS} merger was corroborated to be the origin of \ac{sGRB}, which had been discussed for a long time~\cite{CBC-GRB}. The coordinated observation by different means of astronomical signals, for example, \ac{GW} and \ac{EM} wave is so-called multi-messenger astronomy. GW170817 is one of the successful cases of multi-messenger astronomy. By multi-wavelength observations, information of systems is much more increased. Furthermore, the \ac{O3} with improved sensitivity was started in 2019 and many observations with higher sensitivity have been already planned. \ac{GW} observation is expected to play a more important role in physics.

So far, all \ac{CBC} events have been detected after the merger of the two bodies. Unlike \ac{EM} waves, \ac{GW} is emitted during inspiral. Thus if \ac{CBC} signals are sufficiently loud, one can detect them before the merger by accumulating enough \ac{SNR} to detect, which is called early warning~\cite{early_warning}. This could bring scientific benefits for multi-messenger astronomy because one can prepare for transient events and observe precursor events. For example, there are prompt optical flash from \ac{BNS}~\cite{prompt_flash}, and characteristic \ac{EM} emission from tidal disruption of \ac{NSBH} before merger (precursor)~\cite{NSBH_EMemission}. There should be many unexpected events over multi-wavelength.

In an early warning context, location estimates can be iteratively improved.  There are currently two stages of refinement, BAYESTAR~\cite{bayestar, bayestar3d} which takes about $\SI{3}{s}$, and LALInference~\cite{LALInference} which takes hours to days. However, there is a need for a still faster location estimate even at the expense of localization accuracy. A rough location estimate available in $\mathcal{O}(\SI{100}{ms})$ could trigger the slewing of fast facilities like Cherenkov telescopes, allow better data retention decisions by low-frequency radio facilities, and it might even be used to inform the ranking statistic and improve \ac{GW} signal identification. The speed difference between the BAYESTAR and LALInference algorithms is mainly due to LALInference marginalizing over intrinsic parameters such as source mass, whereas these are fixed near the peak of the likelihood by BAYESTAR. This brights with it a small loss of accuracy. In this work we present a new method that reduces the localization time further by fixing additional parameters such as the distance to the source and orbit inclination. This brings with it a yet further loss of accuracy, but provides an algorithm that fills a niche for ultra-fast location estimates.

If the readers are interested in other rapid localization methods, there is similar work~\cite{rapid_pseudo_bayestar} with a similar motivation. This work is motivated by BAYESTAR~\cite{bayestar, bayestar3d}, which is the perfectly different approach with the our new method motivated by Excess power method. Although the motivation of the our new method is similar to the one of~\cite{rapid_pseudo_bayestar}, we compare performances between the new method and BAYESTAR, because BAYESTAR is worked in the current detection pipeline.


\section{Notation}
We have \ac{GW} detectors, LIGO-Hanford, LIGO-Livingston, Virgo, KAGRA~\cite{KAGRA1, KAGRA2} and so on. Each detector outputs time series data. From here, those are written as a vector:
\eq{
	\bm{d}[j] = \left( d_1[j], d_2[j], \cdots, d_D[j] \right)^T
}
where $j$ is an integer index enumerating discrete time, and $D$ is the number of detectors such that $D\geq 2$. Also corresponding antenna responses~\cite{maggiore1} with polarization angle $\psi$ for \ac{GW} source direction $\bm{\Omega}$ are written as a matrix:
\eq{
	\bm{F}(\bm{\Omega},\psi) = \left(
		\begin{array}{ccc}
			F_{1,+}(\bm{\Omega},\psi) & \cdots & F_{D,+}(\bm{\Omega},\psi) \\
			F_{1,\cross}(\bm{\Omega},\psi) & \cdots & F_{D,\cross}(\bm{\Omega},\psi) 
		\end{array}
		\right)^T
}
We assume that noise is uncorrelated between any pair of the detectors:
\eq{
	\langle \tilde{n}_I[k] \tilde{n}^*_J[k'] \rangle := \frac{1}{2} \delta_{IJ} \frac{\delta_{kk'}}{\varDelta f} S_{n,I}[k]
}
where $S_{nI}$ is noise \ac{PSD} for an $I$-th detector. Using this, complex SNR is defined as follows,
\eq{
	\rho_I[j] &:= (d_I[j+j']|h[j']) \\
	&:= 4 \sum_{k=0}^{N-1} \frac{\tilde{d}_I^*[k] \tilde{h}[k]}{S_{nI}[k]} \exp\left[ 2\pi\iu \frac{j}{N}k \right] \varDelta f
}
where $h = h_+ + \iu h_\cross$ is a template, that is, a theoretical waveform normalized by $(h[j]|h[j]) = 2$. Fourier transformation is given by
\eq{
	\tilde{d}_\alpha[k] &:= \sum_{j=0}^{N-1} d_\alpha[j]\ \exp\left[ -2\pi\iu \frac{k}{N} j \right] \varDelta t
}
and
\eq{
	d_\alpha[j] &:= \sum_{k=0}^{N-1} \tilde{d}_\alpha[k]\ \exp\left[ 2\pi\iu \frac{k}{N} j \right] \varDelta f
}
where $\varDelta t \varDelta f = 1/N$. Then, \ac{SNR} \ac{PSD} is defined as noise \ac{PSD} of the output of the matched filter (See \appref{SEC:PSD_SNR})
\eq{
	\left. \av{ \tilde{\rho}_I[k] \tilde{\rho}^*_J[k'] } \right|_{d=n} &= \frac{1}{2} \delta_{IJ} \frac{\delta_{kk'}}{\varDelta f} S_{\rho,I}[k] \\
	S_{\rho,I}[k] &:= 4\,\frac{\tilde{h}[k] \tilde{h}^*[k]}{S_{nI}[k]} \label{EQ:S_rho}
}

Later, the data of $I$-th detector on time domain is shifted to represent the data at geocenter. If \ac{GW} comes from $\bm{\Omega}$, the time delay on discrete time is $\tau_I(\bm{\Omega}) := \bm{r_I \cdot \Omega} / (c \varDelta t)$ between $I$-th detector at $\bm{r}_I$ and geocenter. When this delay is applied, the time shifted data on frequency domain should be written as
\eq{
	\tilde{\bm{d}}[k;\bm{\Omega}] &= \left(
		\begin{array}{c}
			\tilde{T}_1[k; \bm{\Omega}] \tilde{d}_1[k]\\
			\vdots \\
			\tilde{T}_D[k; \bm{\Omega}] \tilde{d}_D[k]
		\end{array}
		\right)
}
where
\eq{
	\tilde{T}_I[k;\bm{\Omega}] &:= \exp\left[ 2\pi\iu \frac{k}{N} \tau_I(\bm{\Omega}) \right]
}
is time delay operator. For this data, time shifted \ac{SNR} series are written as $\bm{\rho}[j;\bm{\Omega}] := (\bm{d}[j+j';\bm{\Omega}] | h[j'])$.

Similarly, whitened \ac{SNR} and whitened time shifted \ac{SNR} are introduced
\eq{
	\hat{\tilde{\bm{\rho}}}[k] = \left(
		\begin{array}{c}
			\tilde{\rho}_1[k] / \sqrt{S_{\rho,1}[k]} \\
			\vdots \\
			\tilde{\rho}_D[k] / \sqrt{S_{\rho,D}[k]}
		\end{array}
		\right) \label{EQ:def_whitened_SNR}
}
\eq{
	\hatilde{\rho}_I[k;\bm{\Omega}] &= \tilde{T}_I[k;\bm{\Omega}] \hatilde{\rho}_I[k]
}
and whitened antenna response:
\eq{
	\bm{\hat{F}}[k; \bm{\Omega},\psi] :=& \left( \bm{\hat{F}_+}[k; \bm{\Omega}, \psi], \bm{\hat{F}_\cross}[k; \bm{\Omega}, \psi] \right) \\
		:=& \left(
		\begin{array}{c}
			F_{1,+}(\bm{\Omega},\psi)/S_{n,1}[k] \sqrt{S_{\rho,1}[k]},\\
			F_{1,\cross}(\bm{\Omega},\psi)/S_{n,1}[k] \sqrt{S_{\rho,1}[k]},
		\end{array}
		\right. \nonumber \\
		& \left.
		\begin{array}{cc}
			 \qquad \cdots, & F_{D,+}(\bm{\Omega},\psi)/S_{n,D}[k] \sqrt{S_{\rho,D}[k]} \\
			 \qquad \cdots, & F_{D,\cross}(\bm{\Omega},\psi)/S_{n,D}[k] \sqrt{S_{\rho,D}[k]}
		\end{array}
		\right)^T
}
Under this notation, if \ac{GW} is contained in data, a whitened \ac{SNR} frequency series is written as
\eq{
	\hatilde{\rho}_I[k;\bm{\Omega}] =& \left( \hat{F}_{I,+}[k; \bm{\Omega}, \psi], \hat{F}_{I,\cross}[k; \bm{\Omega}, \psi] \right) \begin{pmatrix} \tilde{h}_+[k] \\ \tilde{h}_\cross[k] \end{pmatrix} \tilde{h}^*_{\mathrm{GW}}[k] \nonumber\\
		&+ \frac{\tilde{n}^*_I[k] (\tilde{h}_+[k] + \tilde{h}_\cross[k])}{S_{n,I}[k]} \frac{\exp\left[ 2\pi\iu\frac{k}{N} \tau_I(\bm{\Omega}) \right]}{\sqrt{S_{\rho,I}}}
}
where $h_\mathrm{GW} = h_{\mathrm{GW}, +} + h_{\mathrm{GW}, \cross}$ is the true \ac{GW} strain in nature with $(h_+, h_{\mathrm{GW}, \cross}) = (h_\cross, h_{\mathrm{GW}, +}) = 0$. We will use this series for the localization instead of the strain data.


\section{Compact Binary Coalescence parameterized Likelihood} \label{SEC:CBC_parameterized_Likelihood}
Here, we assume that the source is \ac{CBC}, therefore the two polarizations are related by $\tilde{h}_\cross = \iu\beta\tilde{h}_+$ where $\beta = \frac{2\cos\iota}{1+\cos^2\iota}$ with the inclination $\iota$. Then if the noise is Gaussian, the probability of obtaining $\bm{\hatilde{\rho}}$ in the presence of $\tilde{h}_{\mathrm{GW}}$ with given parameters $\bm{\Omega}, \beta, \psi$ is
\eq{
	&p(\bm{\hatilde{\rho}}|\bm{\Omega}, \tilde{h}_{\mathrm{GW}}, \beta, \psi) \nonumber\\
	\propto& \exp\left[ -2 \sum_{k=0}^{N-1} \left| \bm{\hatilde{\rho}}[k; \bm{\Omega}] \right.\right.\nonumber\\
	& \quad \left.\left. - \left( \bm{\hat{F}_+}[k; \bm{\Omega}, \psi] + \iu\beta \bm{\hat{F}_\cross}[k; \bm{\Omega}, \psi] \right) \tilde{h}_+[k] \tilde{h}_{\mathrm{GW}}[k]  \right|^2 \varDelta f \right]
}
Since $\tilde{h}_{\mathrm{GW}}$ is not known a priori, this probability should be maximized with respect to $\tilde{h}_{\mathrm{GW}}$. This was solved by Sutton et al. in~\cite{Sutton} for the case of general \ac{GW}s. This probability is maximized by $\tilde{h}_+ \tilde{h}_{\mathrm{GW}} = \left[ \left| \bm{\hat{F}_+} \right|^2 + \beta^2 \left| \bm{\hat{F}_\cross} \right|^2\right]^{-1} \left( \bm{\hat{F}_+} + \iu\beta \bm{\hat{F}_\cross} \right)^\dagger \bm{\hatilde{\rho}}$, which is in effect maximizing the probability over the distance to the source:
\eq{
	p(\bm{\hatilde{\rho}}|\bm{\Omega}, \beta, \psi) &\propto \exp\left[ 2 \sum_{k=0}^{N-1} \bm{\hatilde{\rho}}^\dagger[k;\bm{\Omega}] \bm{\hat{P}}[k; \bm{\Omega}, \beta, \psi] \bm{\hatilde{\rho}}[k;\bm{\Omega}] \right] \label{EQ:bare_probability}
}
where
\begin{widetext}
	\eq{
		\bm{\hat{P}}[k; \bm{\Omega}, \beta, \psi] &:= \frac{(\bm{\hat{F}}_+[k; \bm{\Omega}, \psi] + \iu\beta \bm{\hat{F}}_\cross[k; \bm{\Omega}, \psi]) \otimes (\bm{\hat{F}}_+[k; \bm{\Omega}, \psi] - \iu\beta \bm{\hat{F}}_\cross[k; \bm{\Omega}, \psi])}{ \left| \bm{\hat{F}_+}[k; \bm{\Omega}, \psi] \right|^2 + \beta^2 \left| \bm{\hat{F}_\cross}[k; \bm{\Omega}, \psi] \right|^2  } \label{EQ:def_P}
	}
\end{widetext}
is a projection operator to extract the \ac{GW} component from the data\footnote{$\bm{\hat{P}}$ satisfies $\bm{\hat{P}} \bm{\hat{P}} = \bm{\hat{P}}$ and $\bm{\hat{P}} (\bm{\hat{F}_+} + \iu\beta \bm{\hat{F}_\cross}) \tilde{h}_+ \tilde{h}_{\mathrm{GW}} = (\bm{\hat{F}_+} + \iu\beta \bm{\hat{F}_\cross}) \tilde{h}_+ \tilde{h}_{\mathrm{GW}}$. Therefore $\bm{\hat{P}}$ is a projection operator to extract \ac{GW} contributions from the given data. Since $\bm{\hat{P}}$ is constructed by only $\bm{\hat{F}_+} + \iu\beta \bm{\hat{F}_\cross}$, the dimension of the \ac{GW} space where $\bm{\hat{P}}$ project data onto is one (see \figref{FIG:projection}).}. Also, $\otimes$ is direct product.
\myfig{concept_projection.png}{Schematic representation of the concept of $\bm{\hat{P}}$ for a three detectors case, which is in a \ac{SNR} data space spanned by those detectors. Since $\hatilde{\rho}_I$ is complex, \ac{SNR} data space has twice dimensions of the number of detectors, although the half dimensions are omitted in the above picture. Red vector $\bm{\hatilde{\rho}}$ is an observed data in the data space. Blue dashed line is the \ac{GW} space.}{FIG:projection}

Similarly, \eqref{EQ:bare_probability} should be maximized with respect to the remaining parameters $\beta, \psi$. The solutions are $\beta = 0, \pm 1$ with $\psi$ depending on \ac{SNR} data $\bm{\hatilde{\rho}}$ respectively. We cannot estimate which condition is a global maximum before observations, therefore we simply marginalize \eqref{EQ:bare_probability} over $\beta = 0, \pm1$. From \appref{SEC:prior_of_beta}, a prior of $\beta$ for the detected \ac{GW}s is $p(\beta|\mathrm{detect}) \sim (\delta_{\beta=+1} + \delta_{\beta=-1}) / 2$:
\eq{
	p(\bm{\hatilde{\rho}}|\bm{\Omega}, \psi) \propto& \sum_{\beta=0,\pm 1} p(\bm{\rho}|\bm{\Omega}, \beta, \psi) p(\beta|\mathrm{detect})\\
		\propto& \sum_{\beta=\pm1} \exp\Biggl[ 2 \sum_{IJ\in IFO} \sum_{k=0}^{N-1} \hatilde{\rho}_I^*[k] \hatilde{\rho}_J[k] \nonumber\\
		& \quad \times \hat{P}_{IJ}[k;\bm{\Omega}, \beta, \psi=0] \tilde{T}_I^*[k;\bm{\Omega}] \tilde{T}_J[k;\bm{\Omega}] \Biggr] \label{EQ:basic_probability}
}
where the projection operator $\bm{\hat{P}}$ for $\beta=\pm 1$ can neglect a dependence of $\psi$, so that $\psi = 0$ is set\footnote{$\bm{\hat{F}_+} \pm \iu\bm{\hat{F}_\cross} \rightarrow \left( \bm{\hat{F}_+} \pm \iu\bm{\hat{F}_\cross} \right) \exp\left( \pm \iu\psi \right)$ is satisfied by rotating $\psi$; $\bm{\hat{F}_+} \rightarrow \bm{\hat{F}_+} \cos2\psi + \bm{\hat{F}_\cross} \sin2\psi$, $\bm{\hat{F}_\cross} \rightarrow -\bm{\hat{F}_+} \sin2\psi + \bm{\hat{F}_\cross} \cos2\psi$. This phase factor is canceled in $\bm{\hat{P}}$}.

Each of the exponential terms in the sum in \eqref{EQ:basic_probability} contains factors that depend on data from single detectors ($I = J$) and factors that depend on data from pairs of detectors ($I \neq J$). We call these auto- and cross-correlation terms, respectively, and the probability can be written $p(\bm{\hatilde{\rho}}|\bm{\Omega}) = p^{\mathrm{auto}}(\bm{\hatilde{\rho}}|\bm{\Omega}) p^{\mathrm{cross}}(\bm{\hatilde{\rho}}|\bm{\Omega})$. From \appref{SEC:correlation_energy}, $p^\mathrm{cross}$ is the probability of obtaining \ac{GW} signal energy $E_\mathrm{gw}$ when total signal energy is $E_\mathrm{gw} + E_\mathrm{noise}$, and $p^\mathrm{auto}$ is that of obtaining the total signal energy $E_\mathrm{gw} + E_\mathrm{noise}$ when the \ac{GW} signal energy is $E_\mathrm{gw}$ in the meaning of expectation value. We are not interested in noise signal energy so that $p^{\mathrm{cross}}$ should be used to make the sky maps\footnote{\eqref{EQ:bare_p_cross_rho_Omega} can be recognized as matched filter when recognizing $\rho_I$ as data, and $\hat{P}_{IJ}\rho_J$ as template.}.
\eq{
	p^{\mathrm{cross}}(\bm{\hatilde{\rho}}|\bm{\Omega}) \propto& \sum_{\beta=\pm1} \exp\left[ 4 \Re \sum_{I>J\in \mathrm{IFO}} \sum_{k=0}^{N-1} \hatilde{\rho}_I^*[k] \hatilde{\rho}_J[k] \right.\nonumber\\
		& \left. \times \hat{P}_{IJ}[k;\bm{\Omega}, \beta, \psi=0] \tilde{T}_I^*[k;\bm{\Omega}] \tilde{T}_J[k;\bm{\Omega}] \right] \label{EQ:bare_p_cross_rho_Omega}
}

Here, if $\bm{\hat{P}}$ is replaced with $\bm{P}(\bm{\Omega}, \beta, \psi=0) := \left. \bm{\hat{P}} \right|_{\bm{\hat{F}} = \bm{F}}$, $P_{IJ} \tilde{T}^*_I \tilde{T}_J$ becomes independent of the \ac{SNR} data, allowing it to be pre-computed for speed\footnote{$\bm{\hat{P}} \rightarrow \bm{P}$ corresponds to an assumption that all detector have same \ac{PSD} because, if so, $\sqrt{S_\rho}$ in denominator and numerator are canceled.}. Following the approach presented in~\cite{Kipp}, we expand the $\rho$-independent factor in spherical harmonics $Y_{lm}$:
\eq{
	p^{\mathrm{cross}}(\bm{\hatilde{\rho}}|\bm{\Omega}) \propto& \sum_{\beta=\pm1} \exp\left[ 4 \Re \sum_{lm} \left\{ \sum_{I>J\in \mathrm{IFO}} \sum_{k=0}^{N-1} \right.\right. \nonumber\\
		& \left.\left. \left( P \tilde{T}^* \tilde{T} \right)_{IJ}^{lm}[k; \beta] \hatilde{\rho}_I^*[k] \hatilde{\rho}_J[k] \right\} Y_{lm}(\bm{\Omega}) \right]
}


\section{Regulator}
In the definition of the whitened \ac{SNR} in \eqref{EQ:def_whitened_SNR} the ratio $\tilde{\rho}[k] / \sqrt{S_\rho[k]}$ is not well defined for all frequency bins $k$. In particular, because inspiral templates have $0$ signal energy above some high-frequency cutoff $S_\rho$ is $0$ for some $k$ and the whitened \ac{SNR} is undefined. In future work a more careful treatment of this problem will be presented, but at present we have found it is sufficient to regulate the instability by multiplying each term in $p^\mathrm{cross}$ by $2\sqrt{S_{\rho I}[k]} \sqrt{S_{\rho J}[k]}$\footnote{\ac{SNR} \ac{PSD} is defined as the double-sided \ac{PSD}. However, on discrete domain, the single-sided \ac{PSD} is used. Then, the factor $2$ is needed.}:
\eq{
	p^{\mathrm{cross}}(\bm{\hatilde{\rho}}|\bm{\Omega}) \propto& \sum_{\beta=\pm1} \exp\left[ 8 \Re \sum_{lm} \left\{ \sum_{I>J\in \mathrm{IFO}} \sum_{k=0}^{N-1} \right.\right. \nonumber\\
		& \left.\left. \left( P \tilde{T}^* \tilde{T} \right)_{IJ}^{lm}[k; \beta] \tilde{\rho}_I^*[k] \tilde{\rho}_J[k] \right\} Y_{lm}(\bm{\Omega}) \right]
}
This regulator corresponds to no whitening process (or flat \ac{SNR} \ac{PSD}), that is, \ac{SNR} frequency series is not normalized, which acts as non-Gaussian noise.


\section{Posterior}
In this paper, uniform prior is assumed to get posterior:
\eq{
	p(\delta, \alpha) = \frac{1}{4\pi} \cos\delta \label{EQ:declination_prior}
}

Hence one gets a below posterior from Bayes' theorem:
\eq{
	p^{\mathrm{cross}}(\bm{\Omega}|\bm{\rho}) \propto p^{\mathrm{cross}}(\bm{\rho}|\bm{\Omega}) p(\delta, \alpha) \label{EQ:sphrad_posterior}
}
We use this probability to produce sky maps.


\section{Results \& Discussion} \label{SEC:results}
We will compare the new method with current methods, BAYESTAR~\cite{bayestar, bayestar3d}.

\subsection{Injection test}
We evaluated the performance from an injection test. The setup is below:
\begin{itemize}
	\item TaylorT4threePointFivePN was injected into \ac{O2} data from $\SI{1186624818}{s}$ to $\SI{1187312718}{s}$ in GPS time, that is, August 13-21 in 2017.
	\item The three detectors, LIGO-Hanford, LIGO-Livingston and Virgo were used.
	\item The component masses are randomly sampled for $m_1, m_2 \in [1.08 M_\odot, 1.58 M_\odot]$ with the mean of $1.33 M_\odot$ and the standard deviation of $0.05 M_\odot$.
	\item no component spins.
	\item The distance was randomly sampled from a log-uniform distribution for $r \in [\SI{20}{Mpc}, \SI{200}{Mpc}]$.
	\item Candidates were selected with satisfying:
	\begin{itemize}
		\item Those are contained within $\SI{1}{s}$ around injected time.
		\item The \ac{SNR}s of more than two detectors are exceeded over $8$.
		\item All detectors are worked on Science mode.
		\item The network \ac{SNR} $\sqrt{\sum_{I\in \mathrm{IFO}} \mathrm{SNR}^2_I}$ is maximized in the candidates.
	\end{itemize}
	\item $935$ injections were used.
\end{itemize}
Under the above setting, complex \ac{SNR} time series are generated in $\SI{0.17}{s}$ around the triggered time when detecting the candidate. \figref{FIG:injection_example} is an example of the localization of the injections.

\begin{figure}[t]
	\centering
	\begin{tabular}{c}
		\begin{minipage}{0.5\linewidth}
			\centering
			\includegraphics[width=\linewidth]{sphrad_inj8709.png}\\
			\includegraphics[width=\linewidth]{sphrad_inj8709_zoom.png}\\
			New method
		\end{minipage}

		\begin{minipage}{0.5\linewidth}
			\centering
			\includegraphics[width=\linewidth]{bayestar_inj8709.png}\\
			\includegraphics[width=\linewidth]{bayestar_inj8709_zoom.png}\\
			BAYESTAR
		\end{minipage}
	\end{tabular}
	\caption{All sky and zoom maps of the localization results of the new method and BAYESTAR~\cite{bayestar, bayestar3d} for one of the injections with $(\mathrm{SNR}_{\mathrm{Hanford}}, \mathrm{SNR}_{\mathrm{Livingstone}}, \mathrm{SNR}_{\mathrm{Virgo}}) = (25.2, 44.4, 5.97)$, $m_1 = 1.29 M_\odot, m_2 = 1.30 M_\odot$ and no spin at $\SI{1187034141.975986779}{s}$ at geocenter. the right ascension and the declination are respectively $\SI{9.4}{hour}$ and $\SI{13}{deg}$ marked by a star in all sky maps and a blue plus in zoom maps. Purple line is a $90\%$ contour whose region size is $\SI{40}{deg}^2$ for the new method and $\SI{4}{deg}^2$ for BAYESTAR. Both methods have the true direction inside the $90\%$ contour.}
	\label{FIG:injection_example}
\end{figure}


\subsubsection{Consistency}
From the above complex \ac{SNR} time series, We produce skymaps and a p-p plot (\figref{FIG:pp-plot}) for the new method and BAYESTAR~\cite{bayestar, bayestar3d}. From the definition of $p$ value, the fraction of the injections with a $p$ from the peak of maps to the injected direction should be equal to the $p$, that is, the cumulative lines should be on the diagonal. From \figref{FIG:pp-plot}, the average of the cumulative line of the new method is on the diagonal. Then, the average of the new method is statistically consistent. Nevertheless, parts of the cumulative line are out of the $95\%$ error region. The origin should be from the approximation of $\bm{\hat{P}} \rightarrow \bm{P}$ (see \secref{SEC:CBC_parameterized_Likelihood}), because both methods assumed Gaussian noise and \ac{CBC} waveform, that is, the difference was from the other. That approximation is the sole one to be able to shift the peak of maps.
\myfig{pp-plot.pdf}{p-p plot\cite{url_ligo_skymap} of the new method and BAYESTAR. Cumulative fractions of the injections are a ratio included in a $p$ value. Gray region is error region in $95\%$.}{FIG:pp-plot}


\subsubsection{Accuracy}
\begin{table}[tb]
	\centering
	\caption{Cherenkov Telescope Array has three size telescopes, SST, MST and LST~\cite{CTA}.}
	\begin{tabular}{cccc}
		Name & Field of view & Target energy & Slew speed \\ \hline
		SST & $\SI{8.8}{deg}$ & $1-300\unit{TeV}$ & $\lesssim \SI{1}{min}$ \\
		MST & $7.5-7.7\unit{deg}$ & $\SI{80}{GeV}-\SI{50}{TeV}$ & $< \SI{90}{s}$ \\
		LST & $\SI{4.5}{deg}$ & $\SI{20}{GeV}-\SI{3}{TeV}$ & $< \SI{20}{s}$ \\
	\end{tabular}
	\label{TAB:FOVs}
\end{table}

\figref{FIG:error_size} is the area size distribution recognized as accuracy. Then, the square root of it can be recognized as the opening angle which the telescopes require. From \figref{FIG:error_size}, the new method is about $10$ times less accurate than BAYESTAR~\cite{bayestar, bayestar3d}. Since the area size is $\sim \SI{65}{deg}^2$, the opening angle is $\sim \SI{8}{deg}$. This opening angle is comparable with the field of view of the \ac{CTA} (see \tabref{TAB:FOVs}), so that it is sufficiently accurate for early warning. This worse accuracy than BAYESTAR should be due to the regulator, that is, no whitening approximation (see \secref{SEC:CBC_parameterized_Likelihood}). Thus the new method and BAYESTAR have complementary relation with each other in terms of speed and accuracy. BAYESTAR is more robust than the new method because the new method compares the data of detectors but BAYESTAR does that with the reconstructed waveforms. Therefore BAYESTAR should have better accuracy even if all approximations are removed.
\myfig{error_size.pdf}{Area size distribution of pixels from the peak of maps to the injected direction. Sample ratio is a ratio with an area size.}{FIG:error_size}


\subsubsection{Computational Cost}
The main advantage of the new method is its reduced computational cost and its speed. We measured the relative computational cost of this algorithm and BAYESTAR in single-threaded mode on an Intel Core i7-7600U CPU @ $\SI{2.80}{GHz}$, and also measured the relative run-times of BAYESTAR in that configuration to a fully parallel configuration on an Intel Xeon Gold 6136 CPU @ $\SI{3.00}{GHz}$. Taking the single-threaded run times to be dominated by arithmetic operations (I/O is not significant) then this comparison provides an estimate of the ratio of arithmetic operation count required by the two techniques to produce a location estimate. BAYESTAR is a mature code that has been optimized for the highly parallel Xeon hardware, so we also report a speed comparison of the BAYESTAR code in its production configuration.
\begin{center}
	\centering
	\begin{tabular}{c|cc}
			& New method & BAYESTAR \\ \hline
		single-threaded & $\SI{0.73}{s}$ & $\SI{47}{s}$ \\
		parallelized & - & $\SI{3.3}{s}$
	\end{tabular}
\end{center}


\section{Summary \& Future work} \label{SEC:summary}
We developed a rapid localization method which is $100$ times faster than BAYESTAR~\cite{bayestar, bayestar3d} at the cost of accuracy by an order of magnitude.

Our method assumes the Gaussian noise. To estimate the direction, the new method takes into account the time delays and the amplitude ratios between \ac{SNR} time series from different detectors. By maximizing or marginalizing the probability model \eqref{EQ:bare_probability} and extracting precalculated factors, the number of parameters to estimate during the calculation is reduced, which leads to speeding up the localization.

The new method has three differences from Excess power method~\cite{Sutton} and BAYESTAR~\cite{bayestar, bayestar3d} as follows:
\begin{enumerate}
	\item Compared to BAYESTAR which marginalizes the posterior sky map over distance to source and source orbit inclination, the new method maximizes the posterior with respect to these two parameters. This sacrifices some accuracy in the map, but allows for some expressions to be factored into terms that depend only on data and terms that do not, which can then be pre-computed for greater speed.
	\item \ac{SNR} time series are used instead of strain data. By this, one can generate sky maps optimized for \ac{CBC} templates, and suppress the noise contamination which is orthogonal to the template. This is the difference from Excess power method.
	\item The \ac{CBC} parameterization is used instead of the general parameterization used by Excess power method. By this, our target is only \ac{CBC}, which is same as BAYESTAR. Then, the new method is more accurate than Excess power method, but not BAYESTAR. Also the new method can localize \ac{GW} sources for more than single detector working case but Excess power method can localize for more than double detector working case.
\end{enumerate}

As a potential of further improvements, the approximations applied in \chapref{SEC:CBC_parameterized_Likelihood} are enumerated:
\begin{enumerate}
	\item All detectors have the same \ac{PSD}, that is, neglecting frequency dependence of Projection operator to correct distortions from the antenna responses and extract the \ac{GW} components from data: $\bm{\hat{P}}[k;\bm{\Omega},\beta,\psi=0] \rightarrow \bm{P}(\bm{\Omega},\beta,\psi=0)$.
	\item The \ac{PSD}s of \ac{SNR} time series are flat, that is, no whitening approximation: $\hatilde{\rho}[k] \rightarrow \tilde{\rho}[k]$.
\end{enumerate}
The both approximations are meant to avoid numerical instability. Removing these approximations is future work. First one could shift the peak of maps to the correct peak because our probability should be more affected from the detector with higher sensitivities (more likely). Second one could make error region of sky maps wavy (smaller) because it makes complex phase variation fast, and our probability picks up just real part from the correlations.


\section{Acknowledgments}
This research has made use of data~\cite{open_data_appreciate}, software and/or web tools obtained from the Gravitational Wave Open Science Center (https://www.gw-openscience.org), a service of LIGO Laboratory, the LIGO Scientific Collaboration and the Virgo Collaboration. LIGO is funded by the U.S. National Science Foundation. Virgo is funded by the French Centre National de Recherche Scientifique (CNRS), the Italian Istituto Nazionale della Fisica Nucleare (INFN) and the Dutch Nikhef, with contributions by Polish and Hungarian institutes. This work was supported by the \ac{IGPEES}. We would like to thank Heather Fong, Duncan Meacher, Cody Messick and Leo Pound Singer for teaching us how to use the analyzing software. Also, we are grateful for LIGO-Virgo's computational resources, because the injection data sets are produced with those.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%APPENDIX
\appendix
\section{Derivation of the Power Spectral Density of Signal to Noise Ratio} \label{SEC:PSD_SNR}
We derive the \ac{PSD} of \ac{SNR} with no \ac{GW}. For uncorrelated noise,
\eq{
	& \frac{1}{2} \delta_{I J} \delta(f-f') S_{\rho I}(f) \\
		=& \langle \tilde{\rho}_I(f) \tilde{\rho}^*_J(f') \rangle \\
		=& \int \diff t \diff t' \langle \rho_I(t) \rho_J^*(t') \rangle \e^{-2\pi\iu (ft - f't')} \\
		=& 4 \int \diff t \diff t' \int_{-\infty}^\infty \diff g \diff g' \frac{\langle \tilde{n}_I(g) \tilde{n}_J^*(g') \rangle \tilde{h}(g) \tilde{h}^*(g')}{S_{nI}(g) S_{nJ}(g')} \nonumber \\
		 & \quad \times \e^{-2\pi\iu (ft - f't') + 2\pi\iu (gt - g't')} \\
		=& \frac{\delta_{IJ}}{2} 4 \int_{-\infty}^\infty \diff g \frac{\tilde{h}(g) \tilde{h}^*(g)}{S_{nI}(g)} \delta(f-g) \delta(f'-g) \\
		=& \frac{1}{2} \delta_{IJ} \delta(f-f') \, 4 \, \frac{\tilde{h}(f) \tilde{h}^*(f)}{S_{nI(f)}}
}
Comparing LHS with RHS, \eqref{EQ:S_rho} is obtained.


\section{Prior of \texorpdfstring{$\beta$}{\textbackslash beta}} \label{SEC:prior_of_beta}
The probability of detecting \ac{GW}s with an inclination $\iota$ should be proportional to an observable volume if the number density of \ac{CBC} is uniform.
\eq{
	p(\mathrm{detect}|\iota) &\propto D_{\mathrm{range}}^3(\iota) \propto g^3(\iota)\\
	g(\iota) &:= \left( \frac{ 1 + \cos^2\iota }{2} \right)^2 + \cos^2\iota
}
where $D_\mathrm{range}$ is the range of detectors~\cite{Jolien, maggiore1}. Since our universe should not have special direction, the prior is similar to the one of declination \eqref{EQ:declination_prior}:
\eq{
	p(\iota) \propto \sin\iota
}
From Bayes' theorem, the probability of the inclination $\iota$ for \ac{GW}s to be detected is
\eq{
	p(\iota|\mathrm{detect}) \propto p(\mathrm{detect}|\iota) p(\iota) \propto g^3(\iota) \sin\iota
}

Next, a probability of obtaining $\beta = 2\cos\iota / (1+\cos^2\iota)$ for detected events in general is derived. From $|p(\iota|\mathrm{detect})\,d\iota| = |p(\beta|\mathrm{detect})\,d\beta|$,
\eq{
	p(\beta|\mathrm{detect}) &= p(\iota|\mathrm{detect}) \left| \frac{d\iota(\beta)}{d\beta} \right|\\
		&\propto g^3(\iota(\beta)) \left| \frac{\sin\iota(\beta)}{\beta(1-\beta^2)} \right| \sqrt{\sqrt{1-\beta^2} - (1-\beta^2)}\\
	\iota(\beta) &= \cos^{-1}\left[ \frac{1-\sqrt{1-\beta^2}}{\beta} \right]
}
\myfig{prior_of_beta.pdf}{This is a probability of $\beta$ for detected events, that is $\beta$ vs. $p(\beta|\mathrm{detect})$.}{FIG:prior_of_beta}
\figref{FIG:prior_of_beta} shows $p(\beta|\mathrm{detect})$ as a function of $\beta$. We note that $p(\iota|\mathrm{detect})\,d\iota$ is well-defined, that is,
\eq{
	\infty &> \int_0^\pi p(\iota|\mathrm{detect}) \,\diff \iota\\
		&= \int_{-1}^1 p(\beta|\mathrm{detect}) \,\diff \beta
}
Therefore $p(\beta|\mathrm{detect})$ is normalized. Nevertheless, $p(\beta|\mathrm{detect})$ has strong peak for $\beta = \pm 1$. Hence it is approximated with Kronecker-$\delta$:
\eq{
	p(\beta|\mathrm{detect}) \sim \frac{\delta_{\beta=+1} + \delta_{\beta=-1}}{2}
}
Considered in \secref{SEC:CBC_parameterized_Likelihood}, the probability model is extremized at $\beta = 0$ or $\pm1$. Our purpose is not marginalizing posterior with respect to all $\beta$ but the extrema. Therefore, this approximation is reasonable.

This prior does not mean that one always observe the system from head-on or -off although $\beta=\pm1$ ($\iota=0,\pi$). The interpretation is discussed in \appref{SEC:helicity}.


\section{ \texorpdfstring{$\beta = \pm1$}{\textbackslash beta=\textbackslash pm1} and helicity} \label{SEC:helicity}
For $\beta = \pm1$, we write the projection operator \eqref{EQ:def_P} by introducing new basis which is right- and left-handed one:
\eq{
	\bm{\hat{P}}[k;\bm{\Omega}, \beta, \psi] &= \begin{cases}
		\bm{e}^R \otimes \bm{e}^{R *} \quad \mathrm{for} \quad \beta = +1 &\\
		\bm{e}^L \otimes \bm{e}^{L *} \quad \mathrm{for} \quad \beta = -1 &
		\end{cases}
}
\eq{
	\bm{e}^R[k;\bm{\Omega}] :=& \bm{e}^{L*}[k;\bm{\Omega}] \nonumber \\
		:=& \frac{\bm{\hat{F}}_+[k, \bm{\Omega}, \psi=0] + \iu\bm{\hat{F}}_\cross[k, \bm{\Omega}, \psi=0]}{\sqrt{\left| \bm{\hat{F}_+}[k; \bm{\Omega}, \psi=0] \right|^2 + \left| \bm{\hat{F}_\cross}[k; \bm{\Omega}, \psi=0] \right|^2}} \label{EQ:def_basis_RL}
}
where $*$ is complex conjugate. Therefore the projection operators for $\beta=\pm1$ separate data by chirality or helicity. In this notation, the probability \eqref{EQ:basic_probability} is
\eq{
	p(\bm{\hatilde{\rho} | \bm{\Omega}}) \propto& \exp\left[ 2 \sum_{k=0}^{N-1} \hatilde{\rho}^{R*}[k;\bm{\Omega}] \hatilde{\rho}^{R}[k;\bm{\Omega}] \right] \nonumber \\
		& + \exp\left[ 2 \sum_{k=0}^{N-1} \hatilde{\rho}^{L*}[k;\bm{\Omega}] \hatilde{\rho}^{L}[k;\bm{\Omega}] \right]
}
where
\eq{
	\hatilde{\rho}^{R/L}[k;\bm{\Omega}] := \sum_{I\in\mathrm{IFO}} e_{I}^{R/L*} \tilde{T}_I[k;\bm{\Omega}] \hatilde{\rho}_I[k]
}
$e_I^{R/L}$ is the $I$-th component of the vector $\bm{e}^{R/L}$. This marginalization can be understood from the physical picture which one takes into account those polarization.

$\beta = +1 / -1$ corresponds to $\iota = 0 / \pi$ respectively. Nevertheless, it does not mean that there are observers on the azimuthal axis of the \ac{CBC} system. Since the distance to the \ac{CBC} and the inclination are degenerate, our probability model has lost the information of the inclination when the probability was extremized with respect to $\tilde{h}_\mathrm{GW}$. Thus, $\beta = \pm1$ is just a label of the helicity. One does not have to try to read out anything from the inclination.


\section{Relation between correlations and signal energy} \label{SEC:correlation_energy}
\ac{SNR} is an amplitude ratio between \ac{GW} and noise. \ac{GW} signal energy $E_\mathrm{gw}$ is proportional to the squared amplitude. Thus, introducing a conceptual noise signal energy $E_\mathrm{noise}$ from the dimensional analysis, one can recognize \ac{SNR} as a square root of the signal energy ratio~\cite{maggiore1}.

The expectation values of the correlations with \ac{GW} are:
\eq{
	\av{|\hatilde{\rho}_I|^2[k]} &= \frac{\tilde{h}_\mathrm{GW}[k] \tilde{h}_\mathrm{GW}^*[k]}{S_{n,I}[k]} + \frac{N}{2}\\
		& \propto E_\mathrm{gw} + E_\mathrm{noise}\\
	\av{\hatilde{\rho}_I[k] \hatilde{\rho}_J^*[k]} &= \frac{\tilde{h}_\mathrm{GW}[k] \tilde{h}_\mathrm{GW}^*[k]}{\sqrt{S_{n,I}[k] S_{n,J}[k]}}\\
		& \propto E_\mathrm{gw}
}
where $N$ is the number of bins and the \ac{SNR} is whitened. Therefore the auto-correlations correspond to the total signal energy and the cross-correlations to the \ac{GW} signal energy with respect to the expectation values. Then, $p^{\mathrm{cross}}$ is the conditional probability of observing the \ac{GW} signal energy $E_{\mathrm{gw}}$ when the total signal energy is $E_{\mathrm{gw}} + E_{\mathrm{noise}}$: $p^{\mathrm{cross}}(\bm{\rho}|\bm{\Omega}) = p(\bm{\rho}|\bm{\Omega}) / p^{\mathrm{auto}}(\bm{\rho}|\bm{\Omega}) =: p(E_{\mathrm{gw}}|\bm{\Omega}, E = E_{\mathrm{gw}} + E_{\mathrm{noise}})$. 


% The \nocite command causes all entries in a bibliography to be printed out
% whether or not they are actually referenced in the text. This is appropriate
% for the sample file to show the different styles of references, but authors
% most likely will not want to use it.
%\nocite{*}

\bibliographystyle{unsrt}
\bibliography{references}% Produces the bibliography via BibTeX.

\end{document}
