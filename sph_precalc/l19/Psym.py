import numpy as np
import cmath
import matplotlib
#matplotlib.use("Agg")
from matplotlib import pyplot as plt
import healpy as hp
import time
import itertools

import ConfigParser

from lal import ComputeDetAMResponse, C_SI
import lalsimulation


start = time.time()
config = ConfigParser.ConfigParser()
config.read('setting.ini')
section = "parameter set"

NSIDE = config.getint(section, "nside")
npix = config.getint(section, "npix")
DetectorName = eval(config.get(section, "detectorname"))
n_ifo = config.getint(section, "n_ifo")
l_P = config.getint(section, "l_p")

#
# =======================================================================
#
#				calc P
#
# =======================================================================
#

Detector = np.array([lalsimulation.DetectorPrefixToLALDetector(str(name)) for name in DetectorName])

Projection = np.zeros((npix, n_ifo, n_ifo))
for idx in range(npix):
	theta, phi = hp.pix2ang(NSIDE, idx)

	F = np.array([ComputeDetAMResponse(Detector[index].response, phi, np.pi/2-theta, 0, 0) for index in range(n_ifo)])	# NOTE: argument is radians; 
	Projection[idx] = np.matmul(np.matmul(F, np.linalg.inv(np.matmul(F.T, F))), F.T)

Projection = Projection.transpose(1,2,0)	# Projection[\alpha][\beta][idx]


#
# save Plm
#
# off-diagonal part
for n in itertools.combinations(range(n_ifo), 2):
	#
	# rotate for each baseline
	#
	baseline = lalsimulation.DetectorPrefixToLALDetector(DetectorName[n[0]]).location - lalsimulation.DetectorPrefixToLALDetector(DetectorName[n[1]]).location
	baseline = baseline / np.linalg.norm(baseline)
	theta = np.arccos(baseline[2])
	phi = np.arctan2(baseline[1], baseline[0])

	r = hp.rotator.Rotator(rot=(phi,theta,0), deg=False, eulertype="Y")
	Prot = r.rotate_map(Projection[n[0]][n[1]])
	Plm = hp.sphtfunc.map2alm(Prot, lmax=l_P)

	np.save("Psymlm_%s%s.npy" % (DetectorName[n[0]], DetectorName[n[1]]), Plm)

# diagonal part
for n in range(n_ifo):
	#
	# rotate for each baseline
	#
	baseline = lalsimulation.DetectorPrefixToLALDetector(DetectorName[n]).location
	baseline = baseline / np.linalg.norm(baseline)
	theta = np.arccos(baseline[2])
	phi = np.arctan2(baseline[1], baseline[0])

	r = hp.rotator.Rotator(rot=(phi,theta,0), deg=False, eulertype="Y")
	Prot = r.rotate_map(Projection[n][n])
	Plm = hp.sphtfunc.map2alm(Prot, lmax=l_P)

	np.save("Psymlm_%s%s.npy" % (DetectorName[n], DetectorName[n]), Plm)


end = time.time()
print str(end - start) + " s"
