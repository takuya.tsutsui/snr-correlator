#!/usr/bin/python

import os
import warnings
from optparse import OptionParser
import ConfigParser
import itertools


__author__ = "Duncan Meacher <duncan.meacher@ligo.org>"
__date__ = "2014/07/23"
__version__ = "0.1"

__author__ = "Leo Tsukada <leo.tsukada@ligo.org>"
__date__ = "2018/11/21"
__version__ = "0.2"

# =============================================================================
#
#                               HELP MESSAGES
#
# =============================================================================

# ./list_cbc -h
# Options:
# -h                    print this message
# -s                    seed for random generation
# -t                    starting GPS time
# -d                    average time interval between events
# -r                    sampling rate
# -f                    minimal frequency
# -l                    segment duration
# -S                    number of data segments
# -p                    fraction of BNS
# -P                    fraction of BHNS
# -q                    fraction of BBH
# -Q                    fraction of IMRI
# -j                    fraction of IMBHB_GC
# -J                    fraction of IMBHB_seed
# --nocbc               remove BBH, BHNS and BNS
# --noimbh              remove IMBHB
# -z                    minimal redshift
# -Z                    maximal redshift
# --proba               randomly select z, m1, m2 from distibutions for BBH, BHNS and BNS
# -k                    mass distribution of NSs: 0 fixed value, 1 uniform, 2 Gaussian
# -K                    mass distribution of BHs: 0 fixed value, 1 uniform, 2 Gaussian, 3 exponential, 4 log-flat
# -m                    minimal mass of NS
# -M                     maximal mass of NS
# -a                    mean of the mass distribution of NS
# -b                    standart deviation of the mass distribution of NS
# -n                    minimal mass of BH
# -N                    maximal mass of BH
# -u                    mean of the mass distribution of BH
# -v                    standart deviation of the mass distribution of BH
# -G                    beaming angle in degrees for GRB (BNS and BHNS)

# select ET or ALV (but not both) for sustraction of the detectable events(useful for SGWB studies)
# --ET                  sustract sources detectable by ET
# --ALV                 sustract sources detectable by ALV
# -R                    SNR detection threshold

# for Condor use only, split the data into intervals, each containing -S $value data segments
# -I                    number of intervals (by default 1)
# -i                    # of the interval (by default 0)

# ./simul -h
# Options:
# -h                    print this message
# --verbose             verbose mode
# --ascii               write to ascii files
# --ET                  select ET detectors (cannot select both ET and ALV)
# --ALV                 select ALV detectors
# --virgo               include Virgo in ALV
# --noise               generate noise
# --sgwb                simulate sgwb
# --scbc                simulate stellar cbc
# --imbh                simulate IMBBHs and IMRIs
# --burst               simulate burst
# --agwb                simulate background of sine-gaussians or ringdown
# -s                    seed for noise generation
# -j                    job number
# -J                    start job number
# -t                    start time of the serie
# -l                    segment duration
# -r                    sampling rate of the time serie
# -f                    minimal frequency
# -F                    reference frequency for sgwb
# -o                    reference omega for sgwb
# -I                    power index sgwb
# -H                    Hubble parameter
# -a                    amplitude factor for CBC
# -b                    sine-gaussian or ringdown frequency for agwb. Set to 0 for uniform between 10-500 Hz
# -B                    sine-gaussian or ringdown decay time for agwb
# -m                    duty cycle for agwb
# -w                    waveform for agwb : (0: sine-Gaussian(default) 1: ringdown)
# -R                    working directory

# ./back -h
# Options:
# --print               display omega for the reference frequency
# --average             uses expression of omega averaged over orientation
# --ET                  calculate GW PSD for the 3 ET detectors
# --ALV                 calculate PSD for the 3 ALV detectors
# --noIM                subtract IMBBH and IMRI
# -f                    minimal frequency
# -F                    reference frequency
# -t                    CBC type (0: all 1:BNS 2:BHNS 3:BBH 4:IMRI 5:IMBBH_GC 6:IMBBH_seed,
# -T                    duration
# -a                    amplitude factor
# -H                    Hubble parameter
# -h                    print this message


#
# =============================================================================
#
#                                DIRECTORY TREE
#
# =============================================================================
#

# -RunDir
#     -condor(CondirDir)
#         -err(condor_errDir)
#         -out(condor_outDir)
#         ListGen.dag
#         ListGen.sub
#         SimulGen.dag
#         SimulGen.sub
#         RunAllGen.dag
#     -output(outputDir)
#         -lists(listsDir)
#             list_cbc_*.txt
#             cbc_*.dat
#         -omega(omegaDir)
#             omega.txt
#         *.gwf


#
# =============================================================================
#
#                             COMMNAD LINE OPTIONS
#
# =============================================================================
#


def parse_command_line():
    parser = OptionParser(description=__doc__)

    parser.add_option("-d", "--directory", type="str", metavar="DIRECTORY", default="./runs/", help="Set a directory path where to set up a condor environment")
    parser.add_option("-b", "--back", action="store_true", default=False, help="Docstring for argument option")

    options, _ = parser.parse_args()

    return options


# =============================================================================
#
#                                    MAIN
#
# =============================================================================

if __name__ == "__main__":

    options = parse_command_line()
    warnings.filterwarnings("ignore")

    # Set current working directory
    RunDir = options.directory
    if not os.path.isdir(RunDir):
        os.mkdir(RunDir)
    rmRunDir = "rm -rf %s/*" % (RunDir)
    os.system(rmRunDir)

    print "Running directory:", RunDir
    print "Run list and data generation seperately with ListGen.dag and SimulGen.dag or together with RunAllGen.dag\n"

    ListGen_d = "./Xpsym_diagonal.py"
    ListGen_o = "./Xpsym_offdiagonal.py"
    SimulGen = "./simul"

    #
    # Set up directory paths for condor and output
    #

    condorDir = os.path.join(RunDir, "condor")
    if not os.path.isdir(condorDir):
        os.mkdir(condorDir)

    outputDir = os.path.join(RunDir, "output")
    if not os.path.isdir(outputDir):
        os.mkdir(outputDir)

    condor_errDir = os.path.join(condorDir, "err")
    if not os.path.isdir(condor_errDir):
        os.mkdir(condor_errDir)

    condor_outDir = os.path.join(condorDir, "out")
    if not os.path.isdir(condor_outDir):
        os.mkdir(condor_outDir)

    listsDir = os.path.join(outputDir, "lists")
    if not os.path.isdir(listsDir):
        os.mkdir(listsDir)

    framesDir = os.path.join(outputDir, "frames")
    if not os.path.isdir(framesDir):
        os.mkdir(framesDir)

    #
    # Code inputs arguments
    #

    # list_cbc
    listSeed = 1            # -s seed for random number generation
    gpsStart = 800000000    # -t gps start time
    deltaTint = 72.65        # -d average time interval between events
    fs = 4096               # -r sampling rate (Must be powers of 2)
    flow = 10               # -f minimal frequency
    noiseDur = 2048 * 8        # noise segment duration (Must be powers of 2)
    noiseSegPerFr = 1      # number of noise segments per frame
    # noiseDur * noiseSegPerFr is going to be single segment duration
    nSegments = 1           # -S number of data segments (If using condor, set to 1)
    BNSfrac = 0.296             # -p fraction of BNS
    NSBHfrac = 0.687            # -P fraction of BHNS
    BBHfrac = 0.017             # -q fraction of BBH
    IMRIfrac = 0            # -Q fraction of IMRI
    IMBHB_GCfrac = 0        # -j fraction of IMBHB_GC
    IMBHB_seedfrac = 0      # -J fraction of IMBHB_seed
    zMin = 0                # -z minimal redshift
    zMax = 10               # -Z maximal redshift
    NSmass_dist = 1          # -k mass distribution of NSs: 0 fixed value, 1 uniform, 2 Gaussian
    NSMassMin = 1.         # -m NS minimal mass
    NSMassMax = 2.         # -M NS maximal mass
    NSMassMean = 1.4        # -a mean of the NS mass distribution (only if -k=0 or 2)
    NSMassSD = 0.0          # -b standart deviation of the mass distribution (only if -k=2)
    BHmass_dist = 3          # -K mass distribution of BHs: 0 fixed value, 1 uniform, 2 Gaussian, 3 exponential, 4 log-flat
    BHMassMin = 10          # -n minimal mass(only if -K=1 or 2)
    BHMassMax = 10          # -N maximal mass(only if -K=1 or 2)
    BHMassMean = 10         # -u mean of the mass distribution (only if -K=0 or 2)
    BHMassSD = 0.0          # -v standart deviation of the mass distribution (only if -K=2)
    GRBbeaming = 10         # -G beaming angle in degrees for GRB (BNS and BHNS)
    output_dir = listsDir   # -o output directory

# -----------------------------------------------------------------
    # tsutsui parameter
    config = ConfigParser.ConfigParser()
    config.read("setting.ini")
    section = "parameter set"
    NSIDE = config.getint(section, "nside")
    npix = config.getint(section, "npix")
    DetectorName = eval(config.get(section, "detectorname"))
    n_ifo = config.getint(section, "n_ifo")
    N_T = config.getint(section, "n_t")
    l_T = config.getint(section, "l_t")
    l_P = config.getint(section, "l_p")
    l_X = config.getint(section, "l_x")
    T = config.getint(section, "t")
# -------------------------------------------------------------------

    # select ET or ALV (but not both) for sustraction of the detectable events(useful for SGWB studies)
    Det = "--ALV"           # subtract sources detectable by ALV
    # Det            = "--ET"     # sustract sources detectable by ET
    rhoT = 12                # -R SNR detection threshold

    # for Condor use only, split the data into intervals, each containing -S $value data segments
    frameDur = noiseDur * noiseSegPerFr  # -l total duration for single segment
    NumInt = l_X * N_T             # -I number of intervals (by default 1)
    # IntIndex = 0            # -i of the interval (by default 0)

    ListGenParams = [
        "--proba",      # randomly draw parameters from given distribution\
        "%s" % (Det),    # detector to use SNR computation\
        "--LSO",        # calculate SNR only upto ISCO orbit
        "--simul",      # produce .dat files to run ./simul\
        "--glx-catalog",  # use the Millenium galaxy catalog to draw sources anisotropically\
    ]


    # simul
    mdcSeed = 1             # -s seed for noise generation
    StartJob = 0            # -J start job number
    TotSegments = nSegments * NumInt  # the number of the total jobs
    # if sgwb=True
    RefFreq = 100           # -F reference frequency for sgwb
    RefOmega = 1.e-6        # -o reference omega for sgwb
    PowerIndex = 2. / 3.    # -I power index sgwb
    H0 = 0.7                # -H Hubble parameter
    # if scbc=True
    signalAmp = 1           # -a amplitude factor for CBC
    # if agwb=True
    RingdownFreq = 0        # -b sine-gaussian or ringdown frequency for agwb. Set to 0 for uniform between 10-500 Hz
    RingdownDecay = 0.1     # -B sine-gaussian or ringdown decay time for agwb
    DC = 10                 # -m duty cycle for agwb
    Waveform = 0            # -w waveform for agwb : (0: sine-Gaussian(default) 1: ringdown)

    SimulGenParams = [\
        # "--verbose",    # verbose mode
        # "--ascii",      # write to ascii files
        # "--params",     # write source parameters to file
        "%s" % (Det),   # select detectors
        "--virgo",      # include Virgo in ALV
        # "--null",       # produce null stream for ET (with --ET flag)
        # "--single",     # use simple precision
        "--noise",      # generate noise
        # "--sgwb",       # simulate sgwb
        "--scbc",       # simulate stellar cbc
        # "--imbh",       # simulate IMBBHs and IMRIs
        # "--burst",      # simulate burst
        # "--agwb",       # simulate background of sine-gaussians or ringdown
    ]

    # Condor Settings
    maxJobs = 3000              # Maximum number of condor jobs ever submitted at once
    request_memory_ListGen = 150000      # MB the memory volume to allocate for each SimulGen job
    request_memory_SimulGen = 5000       # MB the memory volume to allocate for each ListGen job
    #accounting_group = "ligo.sim.o2.sgwb.isotropic.stochastic"    # set accunting group.
    accounting_group = "ligo.prod.o3.cbc.pe.lalinferencerapid"	# tsutsui
    print ("Setting an accoundting group... Ask a WG chair which accounting group to use")


# -------------------------------------------------------------------------------------------------------------------------

    # For condor use
    #ListArgs    = "-s %d -t %d -d %f -r %d -f %d -l %d -S %d -p %f -P %f -q %f -Q %f -j %f -J %f -z %f -Z %f -m %f -M %f -a %f -b %f -n %f -N %f -u %f -v %f -G %f %s -R %f -I %d"%(listSeed, gpsStart, deltaTint, fs, flow, frameDur, nSegments, BNSfrac, NSBHfrac, BBHfrac, IMRIfrac, IMBHB_GCfrac, IMBHB_seedfrac, zMin, zMax, NSMassMin, NSMassMax, NSMassMean, NSMassSD, BHMassMin, BHMassMax, BHMassMean, BHMassSD, GRBbeaming, ListGenParams, rhoT, NumInt)
    #ListArgs = "-s %d -t %d -d %f -r %d -f %d -l %d -S %d -p %f -P %f -q %f -Q %f -j %f -J %f -z %f -Z %f -k %d -m %f -M %f -a %f -b %f -K %d -n %f -N %f -u %f -v %f -G %f %s -R %f -I %d -o %s" % (listSeed, gpsStart, deltaTint, fs, flow, frameDur, nSegments, BNSfrac, NSBHfrac, BBHfrac, IMRIfrac, IMBHB_GCfrac, IMBHB_seedfrac, zMin, zMax, NSmass_dist, NSMassMin, NSMassMax, NSMassMean, NSMassSD, BHmass_dist, BHMassMin, BHMassMax, BHMassMean, BHMassSD, GRBbeaming, ' '.join(ListGenParams), rhoT, NumInt, output_dir)

    # off diagonal part
    for IFO in itertools.combinations(DetectorName, 2):	# tsutsui
        ListArgs = "%s %s %d %d %d %d %d %d %d %d" % (IFO[0], IFO[1], NSIDE, npix, n_ifo, N_T, l_T, l_P, l_X, T)	#tsutsui
    
        print "Generating condor/ListGen.sub and condor/ListGen.dag files for condor submision"
    
        ListGenDagFile = os.path.join(condorDir, "ListGen_%s%s.dag" % (IFO[0], IFO[1]))	# tsutsui
        f = open(ListGenDagFile, "w")
    
        for i in xrange(NumInt):

            f.write('JOB %d %s/ListGen_%s%s.sub\n' % (i, condorDir, IFO[0], IFO[1]))	# tsutsui
            f.write('RETRY %d 3\n' % (i))
            f.write('VARS %d l="%d" k="%d"\n' % (i, i//N_T, i%N_T))	# tsutsui
            f.write('\n')
    
        f.write("PARENT " + str(range(NumInt)[0]) + " CHILD " + ' '.join(map(str, range(NumInt)[1:])))
        f.close()
        ListGenSubFile = os.path.join(condorDir, "ListGen_%s%s.sub" % (IFO[0], IFO[1]))
        f = open(ListGenSubFile, "w")
    
        f.write('Executable = %s\n' % (ListGen_o))
        f.write('Arguments = $(l) $(k) %s\n' % (ListArgs))	# tsutsui
        f.write('Output = %s/ListGen_%s%s_$(l)_$(k).out\n' % (condor_outDir, IFO[0], IFO[1]))	# tsutsui
        f.write('Error = %s/ListGen_%s%s_$(l)_$(k).err\n' % (condor_errDir, IFO[0], IFO[1]))	# tsutsui
        f.write('Log = %s/ListGen_%s%s.log\n' % (condorDir, IFO[0], IFO[1]))	# tsutsui
        f.write('request_memory = %d\n' % (request_memory_ListGen))
        f.write('getenv = True\n')
        f.write('Notification = never\n')
        f.write('universe = vanilla\n')
        f.write('+Maxhours = 12\n')
        f.write('accounting_group = %s\n' % (accounting_group))
        f.write('Queue 1\n')
        f.close()
    
        print "ListGen.dag now ready for condor submision. Enter: condor_submit_dag -maxjobs", maxJobs, condorDir + "/ListGen.dag"
        print "To check condor status enter: tail -f ", condorDir + "/ListGen.dag.dagman.out \n"
        print "*** Make sure to submit jobs from the MDC_Generation root directory***"

    # diagonal part	TODO
    for IFO in DetectorName:	# tsutsui
        ListArgs = "%s %s %d %d %d %d %d %d %d %d" % (IFO, IFO, NSIDE, npix, n_ifo, N_T, l_T, l_P, l_X, T)	#tsutsui
    
        print "Generating condor/ListGen.sub and condor/ListGen.dag files for condor submision"
    
        ListGenDagFile = os.path.join(condorDir, "ListGen_%s%s.dag" % (IFO, IFO))	# tsutsui
        f = open(ListGenDagFile, "w")
    
        for i in xrange(NumInt):

            f.write('JOB %d %s/ListGen_%s%s.sub\n' % (i, condorDir, IFO, IFO))	# tsutsui
            f.write('RETRY %d 3\n' % (i))
            f.write('VARS %d l="%d" k="%d"\n' % (i, i//N_T, i%N_T))	# tsutsui
            f.write('\n')
    
        f.write("PARENT " + str(range(NumInt)[0]) + " CHILD " + ' '.join(map(str, range(NumInt)[1:])))
        f.close()
        ListGenSubFile = os.path.join(condorDir, "ListGen_%s%s.sub" % (IFO, IFO))
        f = open(ListGenSubFile, "w")
    
        f.write('Executable = %s\n' % (ListGen_d))
        f.write('Arguments = $(l) $(k) %s\n' % (ListArgs))	# tsutsui
        f.write('Output = %s/ListGen_%s%s_$(l)_$(k).out\n' % (condor_outDir, IFO, IFO))	# tsutsui
        f.write('Error = %s/ListGen_%s%s_$(l)_$(k).err\n' % (condor_errDir, IFO, IFO))	# tsutsui
        f.write('Log = %s/ListGen_%s%s.log\n' % (condorDir, IFO, IFO))	# tsutsui
        f.write('request_memory = %d\n' % (request_memory_ListGen))
        f.write('getenv = True\n')
        f.write('Notification = never\n')
        f.write('universe = vanilla\n')
        f.write('+Maxhours = 12\n')
        f.write('accounting_group = %s\n' % (accounting_group))
        f.write('Queue 1\n')
        f.close()
    
        print "ListGen.dag now ready for condor submision. Enter: condor_submit_dag -maxjobs", maxJobs, condorDir + "/ListGen.dag"
        print "To check condor status enter: tail -f ", condorDir + "/ListGen.dag.dagman.out \n"
        print "*** Make sure to submit jobs from the MDC_Generation root directory***"
# ------------------------------------------------------------------------------------------------------------------------------------

        if options.back:
            omegaDir = os.path.join(outputDir, "omega")
            if not os.path.isdir(omegaDir):
                os.mkdir(omegaDir)
    
            # Input arguments for back
            Type = 0                # CBC type (0: all 1:BNS 2:BHNS 3:BBH 4:IMRI 5:IMBBH_GC 6:IMBBH_seed)
            Duration = NumInt * nSegments * frameDur  # duration
            backParams = "--print %s -i %s/list_cbc.txt -o %s" % (Det, listsDir, omegaDir)
            BackExecute = "./back -f %d -F %d -t %d -T %d -a %f %s" % (flow, RefFreq, Type, Duration, signalAmp, backParams)
    
            print "Once all lists have been generated calculate background energy density with the following commands:\n"
            print "cat ", listsDir, "/list_cbc_*.txt > ", listsDir, "/list_cbc.txt"
            print BackExecute
