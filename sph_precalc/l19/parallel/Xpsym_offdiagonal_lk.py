#!/usr/bin/env python
import numpy as np
import healpy as hp
import time
import itertools
from sympy.physics.wigner import gaunt
import ConfigParser

from lal import ComputeDetAMResponse, C_SI
import lalsimulation


start = time.time()
config = ConfigParser.ConfigParser()
config.read('setting.ini')
section = "parameter set"

NSIDE = config.getint(section, "nside")
npix = config.getint(section, "npix")
DetectorName = eval(config.get(section, "detectorname"))
n_ifo = config.getint(section, "n_ifo")
N_T = config.getint(section, "n_t")
l_T = config.getint(section, "l_t")
l_P = config.getint(section, "l_p")
l_X = config.getint(section, "l_x")
T = config.getint(section, "t")


#
# =======================================================================
#
#				calc X
#
# =======================================================================
#

# off-diagonal part
for IFO in itertools.combinations(DetectorName, 2):
	X_jk1 = 1j*np.zeros((hp.sphtfunc.Alm.getsize(l_X), T, N_T))	# jk plane
	X_jk2 = 1j*np.zeros((hp.sphtfunc.Alm.getsize(l_X), T, N_T))	# jk plane
	X_kk  = 1j*np.zeros((hp.sphtfunc.Alm.getsize(l_X), N_T, N_T))	# kk plane
	for l in range(l_X+1):
		for k in range(N_T):
			X_jk1 += np.load("X_jk%s_lm_%s%s_l%dk%d.npy" % (IFO[0], IFO[0], IFO[1], l, k))
			X_jk2 += np.load("X_jk%s_lm_%s%s_l%dk%d.npy" % (IFO[1], IFO[0], IFO[1], l, k))
			X_kk  += np.load("X_kk_lm_%s%s_l%dk%d.npy"   % (IFO[0], IFO[1],l, k))
	print IFO
	np.save("X_jk%s_lm_%s%s.npy" % (IFO[0], IFO[0], IFO[1]), X_jk1)
	np.save("X_jk%s_lm_%s%s.npy" % (IFO[1], IFO[0], IFO[1]), X_jk2)
	np.save("X_kk_lm_%s%s.npy" % (IFO, IFO), X_kk)
	
end = time.time()
print str(end - start) + " s"

