#!/usr/bin/env python
import numpy as np
import healpy as hp
import time
import itertools
from sympy.physics.wigner import gaunt
import sys

from lal import ComputeDetAMResponse, C_SI
import lalsimulation


def trans2int(x):
	try:
		return int(x) 
	except ValueError:
		return x

#
# =======================================================================
#
#				parameter
#
# =======================================================================
#

args = sys.argv
args = [trans2int(x) for x in args]
# args[1] = l
# args[2] = k
# args[3] = DetectorName_1
# args[4] = DetectorName__2
# args[8] = N_T
# args[9] = l_T
# args[10]= l_P
# args[11]= l_X
# args[12]= T

start = time.time()


#
# =======================================================================
#
#				calc X
#
# =======================================================================
# j,k_iter

# off-diagonal part
# jk plane
Plm = np.load("Psymlm_%s%s.npy" % (args[3], args[4]))
X_jk_1 = 1j*np.zeros((hp.sphtfunc.Alm.getsize(args[11]), args[12], args[8]))	# j-k[0] plane
X_jk_2 = 1j*np.zeros((hp.sphtfunc.Alm.getsize(args[11]), args[12], args[8]))	# j-k[1] plane

for j in range(-args[12]/2,args[12]/2+1):
	Tlm_1_1 = np.load("Tsymlm_%s%s_%d.npy" % (args[3], args[4], j-((args[8]-1)/2-args[2])))	# k[0] = k
	Tlm_1_2 = np.load("Tsymlm_%s%s_%d.npy" % (args[4], args[3], j- (args[8]-1)/2))		# k[1] = 0
	Tlm_2_1 = np.load("Tsymlm_%s%s_%d.npy" % (args[3], args[4], j- (args[8]-1)/2))		# k[0] = 0
	Tlm_2_2 = np.load("Tsymlm_%s%s_%d.npy" % (args[4], args[3], j-((args[8]-1)/2-args[2])))	# k[1] = k

	for p in range(0, args[11]+args[9]+1):	# restricted by below if sentense.
		for l_1 in range(abs(args[1]-p)+(args[1]+abs(args[1]-p)+p)&1, min(args[9],args[1]+p)+1, 2):
			if p < abs(args[1]-l_1) or p > abs(args[1]+l_1):
				continue

			for l_p in range(args[10]+1):
				for l_2 in range(abs(l_p-p)+(l_p+abs(l_p-p)+p)&1, min(args[9],l_p+p)+1, 2):
					if p < abs(l_p-l_2) or p > abs(l_p+l_2):
						continue

					X_jk_1[hp.sphtfunc.Alm.getidx(args[11],args[1],0)][j][args[2]] += Plm[hp.sphtfunc.Alm.getidx(args[10],l_p,0)] * Tlm_1_1[hp.sphtfunc.Alm.getidx(args[9], l_1, 0)] * Tlm_1_2[hp.sphtfunc.Alm.getidx(args[9], l_2, 0)] * gaunt(args[1], l_1, p, 0, 0, 0, prec=64) * gaunt(l_p, l_2, p, 0, 0, 0, prec=64)
					X_jk_2[hp.sphtfunc.Alm.getidx(args[11],args[1],0)][j][args[2]] += Plm[hp.sphtfunc.Alm.getidx(args[10],l_p,0)] * Tlm_2_1[hp.sphtfunc.Alm.getidx(args[9], l_1, 0)] * Tlm_2_2[hp.sphtfunc.Alm.getidx(args[9], l_2, 0)] * gaunt(args[1], l_1, p, 0, 0, 0, prec=64) * gaunt(l_p, l_2, p, 0, 0, 0, prec=64)

	print "jk : j = %d" % j

np.save("X_jk%s_lm_%s%s_l%dk%d.npy" % (args[3], args[3], args[4], args[1], args[2]), X_jk_1)
np.save("X_jk%s_lm_%s%s_l%dk%d.npy" % (args[4], args[3], args[4], args[1], args[2]), X_jk_2)


# kk plane
X_kk = 1j*np.zeros((hp.sphtfunc.Alm.getsize(args[11]), args[8], args[8]))	# k[0]-k[1] plane
for k in range(args[8]):	# one k is remnant
	# true param for X_{\alpha \beta k_1 k_2}^{lm}
	# -----------------------
	# dummy param
	Tlm_1 = np.load("Tsymlm_%s%s_%d.npy" % (args[3], args[4], k-(args[8]-1)/2))
	Tlm_2 = np.load("Tsymlm_%s%s_%d.npy" % (args[4], args[3], args[2]-(args[8]-1)/2))

	for p in range(0, args[11]+args[9]+1):	# restricted by below if sentense.
		for l_1 in range(abs(args[1]-p)+(args[1]+abs(args[1]-p)+p)&1, min(args[9],args[1]+p)+1, 2):
			if p < abs(args[1]-l_1) or p > abs(args[1]+l_1):
				continue

			for l_p in range(args[10]+1):
				for l_2 in range(abs(l_p-p)+(l_p+abs(l_p-p)+p)&1, min(args[9],l_p+p)+1, 2):
					if p < abs(l_p-l_2) or p > abs(l_p+l_2):
						continue

					X_kk[hp.sphtfunc.Alm.getidx(args[11],args[1],0)][k][args[2]] += Plm[hp.sphtfunc.Alm.getidx(args[10],l_p,0)] * Tlm_1[hp.sphtfunc.Alm.getidx(args[9], l_1, 0)] * Tlm_2[hp.sphtfunc.Alm.getidx(args[9], l_2, 0)] * gaunt(args[1], l_1, p, 0, 0, 0, prec=64) * gaunt(l_p, l_2, p, 0, 0, 0, prec=64)

	print "kk : k[0] = %d" % k

np.save("X_kk_lm_%s%s_l%dk%d.npy" % (args[3], args[4], args[1], args[2]), X_kk)



"""
for n in itertools.combinations_with_replacement(range(n_ifo),2):	# \alpha, \beta
	Plm = np.load("Plm_%s%s.npy" % (str(DetectorName[n[0]]), str(DetectorName[n[1]])))
	X = np.zeros((hp.sphtfunc.Alm.getsize(l_X),N_T,N_T))
	for l in range(l_X+1):
		for m in range(l+1):	# Only positive is OK because the map is real.
			# file name
			# ---------
			# index
			for k in itertools.product(xrange(N_T), xrange(N_T)):	# k_1, k_2
				# true param for X_{\alpha \beta k_1 k_2}^{lm}
				# -----------------------
				# dummy param
				for j in range(-T/2,T/2+1):
					Tlm_1 = np.load("Tlm_%s_%d.npy" % (str(DetectorName[n[0]]), j-k[0]-(N_T-1)/2))
					Tlm_2 = np.load("Tlm_%s_%d.npy" % (str(DetectorName[n[1]]), j-k[1]-(N_T-1)/2))

					for l_p in range(l_X+1):
						for m_p in range(-l_p, l_p+1):
							for l_1 in range(l_X+1):
								for m_1 in range(-l_1, l_1+1):
									for l_2 in range(l_X+1):
										for m_2 in range(-l_2, l_2+1):
											term = 0
											for p in range(l_X):
												factor = 0
												for q in range(l_X):
													factor += wigner_3j(l,l_1,p,m,m_1,q) * wigner_3j(l_p,l_2,p,m_p,m_2,q)

												term += factor * wigner_3j(l,l_1,p,0,0,0) * wigner_3j(l_p,l_2,p,0,0,0) * (2*p+1)

											term *= np.sqrt((2*l+1)*(2*l_1+1)*(2*l_2+1)*(2*l_p+1)) / (4*np.pi)

											if m_p < 0:
												m_p = -m_p
												term *= (-1)**m_p * np.conjugate(Plm[hp.sphtfunc.Alm.getidx(l_X, l_p, m_p)])
											else:
												term *= Plm[hp.sphtfunc.Alm.getidx(l_X, l_p, m_p)]

											if m_1 < 0:
												m_1 = -m_1
												term *= (-1)**m_p * Tlm_1[hp.sphtfunc.Alm.getidx(l_X, l_1, m_1)]
											else:
												term *= np.conjugate(Tlm_1[hp.sphtfunc.Alm.getidx(l_X, l_1, m_1)])

											if m_2 < 0:
												m_2 = -m_2
												term *= (-1)**m_p * np.conjugate(Tlm_2[hp.sphtfunc.Alm.getidx(l_X, l_2, m_2)])
											else:
												term *= Tlm_2[hp.sphtfunc.Alm.getidx(l_X, l_2, m_2)]

											X[hp.sphtfunc.Alm.getidx(l_X,l,m)][k[0]][k[1]] += term

						end = time.time()
						print l_p, m_p, end-start

			np.save("Xlm_%s%s.npy" % (DetectorName[n[0]], DetectorName[n[1]]), X)
"""


end = time.time()
print str(end - start) + " s"

