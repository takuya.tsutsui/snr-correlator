import numpy as np
import cmath
import matplotlib
#matplotlib.use("Agg")
from matplotlib import pyplot as plt
import healpy as hp
import time
import itertools

from lal import ComputeDetAMResponse, C_SI
import lalsimulation


start = time.time()


#
# =======================================================================
#
#				calc P
#
# =======================================================================
#

NSIDE = 32
npix = hp.nside2npix(NSIDE)

DetectorName = ("H1", "L1", "V1")
n_ifo = len(DetectorName)

Detector = np.array([lalsimulation.DetectorPrefixToLALDetector(str(name)) for name in DetectorName])

Projection = np.zeros((npix, n_ifo, n_ifo))
for idx in range(npix):
	theta, phi = hp.pix2ang(NSIDE, idx)

	F = np.array([ComputeDetAMResponse(Detector[index].response, phi, np.pi/2-theta, 0, 0) for index in range(n_ifo)])	# NOTE: argument is radians; 
	Projection[idx] = np.matmul(np.matmul(F, np.linalg.inv(np.matmul(F.T, F))), F.T)

Projection = Projection.transpose(1,2,0)	# Projection[\alpha][\beta][idx]


#
# save Plm
#
l_P = 16

for n in itertools.combinations_with_replacement(range(n_ifo), 2):
	Plm = hp.sphtfunc.map2alm(Projection[n[0]][n[1]], lmax=l_P)
	np.save("Plm_%s%s.npy" % (DetectorName[n[0]], DetectorName[n[1]]), Plm)



end = time.time()
print str(end - start) + " s"
