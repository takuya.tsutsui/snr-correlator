import ConfigParser
import healpy as hp

DetectorName = ("H1", "L1", "V1")
n_ifo = len(DetectorName)
NSIDE = 32

config = ConfigParser.ConfigParser()

section = "parameter set"
config.add_section(section)
config.set(section, "NSIDE", NSIDE)
config.set(section, "npix", hp.nside2npix(NSIDE))
config.set(section, "N_T", 7)
config.set(section, "dt", 0.00048828125)
config.set(section, "l_T", 13)
config.set(section, "T_j", 38)
config.set(section, "l_P", 8)
config.set(section, "l_X", 19)
config.set(section, "T", 38)
config.set(section, "DetectorName", DetectorName)
config.set(section, "n_ifo", n_ifo)

with open('setting.ini', 'w') as file:
	config.write(file)

"""
npix = hp.nside2npix(NSIDE)
#N_T = 127		# length of GW170814
#N_T = 37		# ~peak width
#N_T = 16754136225	# Kipp's estimate, upper limit
#N_T = 351	# sinc interpolation; for k_1, k_2
N_T = 7
dt = 0.00048828125	# delta t
l_T = 13		# upper limit for Time delay operator
#l_T = 41001421423	# Kipp's estimate, upper limit
T_j = 38		# integration duration; for j
#l_P = 16		# upper limit for Projection operator
l_P = 8			# upper limit for Projection operator
l_X = 19		# l_X ~ np.sqrt(npix)|NSIDE=32; for all l
T = 38			# integration duration; for j

DetectorName = ("H1", "L1", "V1")
n_ifo = len(DetectorName)
"""
