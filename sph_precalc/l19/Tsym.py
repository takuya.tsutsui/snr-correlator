import numpy as np
import cmath
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
from matplotlib import cm as cm
from matplotlib.backends.backend_pdf import PdfPages
import healpy as hp
import time
import itertools
import ConfigParser

from glue.ligolw import ligolw
from glue.ligolw import lsctables
from glue.ligolw import utils as ligolw_utils
from lal import series as lalseries
from lal import GreenwichMeanSiderealTime, ComputeDetAMResponse, C_SI
import lalsimulation


start = time.time()
config = ConfigParser.ConfigParser()
config.read('setting.ini')
section = "parameter set"

NSIDE = config.getint(section, "nside")
npix = config.getint(section, "npix")
DetectorName = eval(config.get(section, "detectorname"))
n_ifo = config.getint(section, "n_ifo")
l_T = config.getint(section, "l_t")
dt = config.getfloat(section, "dt")
T_j = config.getint(section, "t_j")
N_T = config.getint(section, "n_t")


#
# =======================================================================
#
#				calc T
#
# =======================================================================
#

for name in itertools.combinations(DetectorName, 2):
	Tp = np.zeros(npix)
	Tm = np.zeros(npix)
	location = lalsimulation.DetectorPrefixToLALDetector(str(name[0])).location - lalsimulation.DetectorPrefixToLALDetector(str(name[1])).location	# relative position vector
	location = np.array([0, 0, np.sign(location[2]) * np.linalg.norm(location) / 2])	# set origin
	for j in xrange(N_T+T_j):
		for idx in range(npix):
			theta, phi = hp.pix2ang(NSIDE, idx)

			direction = np.array([np.sin(theta)*np.cos(phi), np.sin(theta)*np.sin(phi), np.cos(theta)])
			delay = np.dot(direction, location) / (C_SI * dt)
			delay = int(round(delay))

			Tp[idx] = np.sinc(j - (N_T-1)/2 - T_j/2 - delay)
			Tm[idx] = np.sinc(j - (N_T-1)/2 - T_j/2 + delay)

		Tlmp = hp.sphtfunc.map2alm(Tp, lmax=l_T, mmax=0)
		Tlmm = hp.sphtfunc.map2alm(Tm, lmax=l_T, mmax=0)
		np.save("Tsymlm_%s%s_%d.npy" % (name[0], name[1], j-(N_T-1)/2-T_j/2), Tlmp)
		np.save("Tsymlm_%s%s_%d.npy" % (name[1], name[0], j-(N_T-1)/2-T_j/2), Tlmm)

for name in DetectorName:
	T = np.zeros(npix)
	location = np.array([0, 0, np.linalg.norm(lalsimulation.DetectorPrefixToLALDetector(str(name)).location)])
	for j in xrange(N_T+T_j):
		for idx in range(npix):
			theta, phi = hp.pix2ang(NSIDE, idx)

			direction = np.array([np.sin(theta)*np.cos(phi), np.sin(theta)*np.sin(phi), np.cos(theta)])
			delay = np.dot(direction, location) / (C_SI * dt)
			delay = int(round(delay))

			T[idx] = np.sinc(j - (N_T-1)/2 - T_j/2 - delay)

		Tlm = hp.sphtfunc.map2alm(T, lmax=l_T, mmax=0)
		np.save("Tsymlm_%s%s_%d.npy" % (name, name, j-(N_T-1)/2-T_j/2), Tlm)


end = time.time()
print str(end - start) + " s"
