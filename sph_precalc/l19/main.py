import numpy as np
import matplotlib
#matplotlib.use("Agg")
from matplotlib import pyplot as plt
import healpy as hp
import time
import itertools
import ConfigParser

import lalsimulation
from lalburst.snglcoinc import TOATriangulator
from glue.ligolw import ligolw
from glue.ligolw import lsctables
from glue.ligolw import utils as ligolw_utils
from lal import series as lalseries
from lal import GreenwichMeanSiderealTime, ComputeDetAMResponse, C_SI



@lsctables.use_in
class ContentHandler(lalseries.PSDContentHandler):
	pass

start = time.time()
config = ConfigParser.ConfigParser()
config.read('setting.ini')
section = "parameter set"

NSIDE = config.getint(section, "nside")
npix = config.getint(section, "npix")
N_T = config.getint(section, "n_t")
l_X = config.getint(section, "l_x")
T = config.getint(section, "t")


#
# =======================================================================
#
#				parse data
#
# =======================================================================
#

xmldoc = ligolw_utils.load_filename("coinc_GW170814.xml", contenthandler=ContentHandler, verbose=True)	# change the file name ###

sngl_inspiral_table = lsctables.SnglInspiralTable.get_table(xmldoc)
DetectorName = tuple(sngl_inspiral_table.getColumnByName("ifo"))
n_ifo = len(DetectorName)

data_tree = np.array([xmldoc.getElementsByTagName(ligolw.LIGO_LW.tagName)[index] for index in range(n_ifo)])
	# data_tree[index_of_detector].array[time,real,complex]
cSNR = np.array([lalseries.parse_COMPLEX8TimeSeries(data_tree[index]) for index in range(n_ifo)])
	# correspond the order of DetectorName to the order of cSNR


"""	# HIDE FOR INJECTION
###########################################
#		dammy data injection
###########################################
# Hide all "HIDE"
GW_theta = 6*np.pi / 12		# set direction of injection
#GW_theta = np.deg2rad(87.66710548408382 +90)		# set direction of injection
#GW_theta = np.deg2rad(60)		# set direction of injection
#GW_theta = np.deg2rad(98.38553865)	# set direction of injection
#GW_theta = 3.0993959216659293	# on ring
GW_phi = 12*np.pi / 12		# set direction of injection
#GW_phi = np.deg2rad(-74.7714174257378 +90)		# set direction of injection
#GW_phi = np.deg2rad(310.78125)		# set direction of injection
#GW_phi = np.deg2rad(19.6875)		# set direction of injection
#GW_phi = -1.5707963267948966	# on ring

Detector = np.array([lalsimulation.DetectorPrefixToLALDetector(str(name)) for name in DetectorName])

# rewrite initial time
GWdirection = np.array([np.sin(GW_theta)*np.cos(GW_phi), np.sin(GW_theta)*np.sin(GW_phi), np.cos(GW_theta)])
initial_time = 0*np.array([cSNR[index].epoch for index in range(n_ifo)])	# reset
initial_time = np.array([np.dot(-GWdirection, Detector[i].location)/C_SI for i in range(n_ifo)])

T_ = len(cSNR[0].data.data)

toa = T_/2 - int(round(min(initial_time)/cSNR[0].deltaT))
#toa = int(round(T_/2.0 + (sum(initial_time) + np.dot(GWdirection, sum([Detector[i].location for i in range(n_ifo)]))/C_SI)/3/cSNR[0].deltaT))

#diff = GreenwichMeanSiderealTime(toa * cSNR[0].deltaT) % (2*np.pi)
diff = 0

F = np.array([ComputeDetAMResponse(Detector[index].response, GW_phi, np.pi/2-GW_theta, 0, diff) for index in range(n_ifo)])	# argument is radians; Set direection

#def h_form(t):
#	return np.exp(-1000*(t - cSNR[0].deltaT * T_/2)**2 + 1j * t*1000) * t*1000

#h = np.array([h_form(index_t * cSNR[0].deltaT) for index_t in range(T_)])	# virtual signal
h_r = np.random.randn(T_)/50
#h_i = np.random.randn(T_)
h_i = h_r

#'''
h_r *= np.array([(np.exp(-(i - T_/2)**2)/np.sqrt(np.pi))*50 for i in range(T_)])
h_i *= np.array([(np.exp(-(i - T_/2)**2)/np.sqrt(np.pi))*50 for i in range(T_)])
#'''

'''	# FOR NO Projection
for index in range(n_ifo):
	cSNR[index].data.data = 0 * cSNR[index].data.data	# reset
	cSNR[index].data.data += h_r
#'''

#'''	# FOR Projection
for index in range(n_ifo):
	cSNR[index].data.data *= 0	# reset
	cSNR[index].data.data += F[index][0]*h_r + 1j*F[index][1]*h_i
#'''

'''	# with noise
for index in range(n_ifo):
	cSNR[index].data.data += np.random.randn(T_)*1 + 1j*np.random.randn(T_)*1
	cSNR[index].data.data /= 2*np.sqrt(2)
#'''
#cSNR[0].data.data += (np.random.rand(T_)-0.5)*2 + 1j*(np.random.rand(T_)-0.5)*2



#"""


#
# =======================================================================
#
#			zero padding data
#
# =======================================================================
#

#
# set initial time of data
#
initial_time = np.array([cSNR[index].epoch for index in range(n_ifo)])	# HIDE FOR INJECTION

#
# arrange data along time
#
far_IFO = np.argmax(initial_time)
near_IFO = np.argmin(initial_time)
for i in range(n_ifo):				# TODO:switch up to the number of IFO
	if i!=far_IFO and i!=near_IFO:
		medium_IFO = i

gap = int(round((initial_time[far_IFO] - initial_time[near_IFO]) / cSNR[0].deltaT))	# always positive, deltaT is same
gap_r = int(round((initial_time[far_IFO] - initial_time[medium_IFO]) / cSNR[0].deltaT))
gap_l = int(round((initial_time[medium_IFO] - initial_time[near_IFO]) / cSNR[0].deltaT))

ZeroPad_cSNR = [np.array([]) for x in range(n_ifo)]
ZeroPad_cSNR[far_IFO] = np.insert(cSNR[far_IFO].data.data, 0, np.zeros(gap))
ZeroPad_cSNR[near_IFO] = np.insert(cSNR[near_IFO].data.data, len(cSNR[near_IFO].data.data), np.zeros(gap))
ZeroPad_cSNR[medium_IFO] = np.insert(cSNR[medium_IFO].data.data, 0, np.zeros(gap_l))
ZeroPad_cSNR[medium_IFO] = np.insert(ZeroPad_cSNR[medium_IFO], len(ZeroPad_cSNR[medium_IFO]), np.zeros(gap_r))


#
# =======================================================================
#
#				calc L
#
# =======================================================================
#

logLlm = 1j*np.zeros(hp.sphtfunc.Alm.getsize(l_X))
mode = 1j*np.empty((6, hp.sphtfunc.Alm.getsize(l_X)))	# delete this after mode decomposition test
i = 0							# delete this after mode decomposition test
modemap = np.empty((6, npix))				# delete this after mode decomposition test

# offdiagonal part
for n in itertools.combinations(range(n_ifo), 2):
	toa = int(round( 0.5*abs(initial_time[n[0]] - initial_time[n[1]]) / cSNR[0].deltaT )) + len(cSNR[0].data.data)/2 + int(round( (min(initial_time[n[0]], initial_time[n[1]]) - min(initial_time))/cSNR[0].deltaT ))
	cutSNR = np.array([ ZeroPad_cSNR[n[i]][toa-(N_T-1)/2:toa+(N_T-1)/2+1].copy() for i in range(2) ])	# pick up data around the time of arival at the center of the baseline
	Xlm = np.load("Xlm_%s%s.npy" % (str(DetectorName[n[0]]), str(DetectorName[n[1]])))
	logLlm_baseline = np.dot(np.dot(Xlm, cutSNR[0]), np.conjugate(cutSNR[1])) + np.dot(np.dot(Xlm, cutSNR[1]), np.conjugate(cutSNR[0]))

	hp.rotate_alm(logLlm_baseline, -GreenwichMeanSiderealTime(toa), 0, 0)		# rotate sky 
	logLlm += logLlm_baseline
	mode[i] = logLlm_baseline	# mode decomposition
	i += 1				# mode decomposition
	print cutSNR


#
# set arrival time at geocenter
#
triangulator = TOATriangulator([lalsimulation.DetectorPrefixToLALDetector(str(name)).location for name in DetectorName], cSNR[0].deltaT*np.ones(n_ifo))
toa = int(round( (triangulator(initial_time)[1] - min(initial_time)) / cSNR[0].deltaT )) + len(cSNR[0].data.data)/2
# diagonal part
logLlm_baseline *= 0j
for n in range(n_ifo):
	cutSNR = ZeroPad_cSNR[n][toa-(N_T-1)/2:toa+(N_T-1)/2+1].copy()
	Xlm = np.load("Xlm_%s%s.npy" % (str(DetectorName[n]), str(DetectorName[n])))
	logLlm_baseline += np.dot(np.dot(Xlm, cutSNR), np.conjugate(cutSNR))
	mode[i] = np.dot(np.dot(Xlm, cutSNR), np.conjugate(cutSNR))	# mode decomposition
	i += 1								# mode decomposition
	print cutSNR

hp.rotate_alm(logLlm_baseline, -GreenwichMeanSiderealTime(toa), 0,0)	# rotate sky 
logLlm += logLlm_baseline
for i in range(3,6):	# mode decompsition
	hp.rotate_alm(mode[i], -GreenwichMeanSiderealTime(toa), 0, 0)

#
# construct logL
#
logLlm *= T/2.
logL = hp.alm2map(logLlm, NSIDE)
mode *= T/2.		# mode decomposition
for i in range(6):	# mode decomposition
	modemap[i] = hp.alm2map(mode[i], NSIDE)


#
# =======================================================================
#
#				plot
#
# =======================================================================
#

#hp.write_map("GWmap.fits", logL)
hp.mollview(logL, rot=(180,0,0), title="Liklihood ratio of GW source location", unit="log(Likelihood ratio)")
hp.graticule()
plt.savefig("map.pdf")


# mode decomposition
plt.clf()
fig, ax = plt.subplots(3,2)
j=0
for IFO in itertools.combinations(range(3),2):
	plt.axes(ax[j,0])
	hp.mollview(modemap[j], title="P_%s%s" % (str(DetectorName[IFO[0]]),str(DetectorName[IFO[1]])), rot=(180,0,0), hold=True)
	hp.graticule()
	j += 1

for j in range(3):
	plt.axes(ax[j,1])
	hp.mollview(modemap[j+3], title="P_%s%s" % (str(DetectorName[j]),str(DetectorName[j])), rot=(180,0,0), hold=True)
	hp.graticule()

plt.savefig("contribution_from_modes.eps")




end = time.time()
print str(end - start) + " s"
