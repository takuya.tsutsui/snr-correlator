import numpy as np
import matplotlib
#matplotlib.use("Agg")
from matplotlib import pyplot as plt
import healpy as hp
import time
import itertools
from sympy.physics.wigner import gaunt
import ConfigParser

from lal import ComputeDetAMResponse, C_SI
import lalsimulation


start = time.time()
config = ConfigParser.ConfigParser()
config.read('setting.ini')
section = "parameter set"

NSIDE = config.getint(section, "nside")
npix = config.getint(section, "npix")
DetectorName = eval(config.get(section, "detectorname"))
n_ifo = config.getint(section, "n_ifo")
N_T = config.getint(section, "n_t")
l_T = config.getint(section, "l_t")
l_P = config.getint(section, "l_p")
l_X = config.getint(section, "l_x")
T = config.getint(section, "t")


#
# =======================================================================
#
#				calc X
#
# =======================================================================
#

ind = np.meshgrid(range(hp.sphtfunc.Alm.getsize(l_X)), range(T), range(N_T), range(N_T), indexing="ij")
for n in itertools.combinations_with_replacement(DetectorName, 2):
	# store the data
	Xlm = 1j*np.zeros((hp.sphtfunc.Alm.getsize(l_X), T, N_T, N_T))	# Xlm[lm][j][k[0]][k[1]]
	Xlm[ind[1] == 0] = np.load("X_kk_lm_%s%s.npy"   %       (n[0], n[1])).reshape(1, len(Xlm[ind[1] == 0]))[0]
	Xlm[ind[2] == 0] = np.load("X_jk%s_lm_%s%s.npy" % (n[0], n[0], n[1])).reshape(1, len(Xlm[ind[2] == 0]))[0]
	Xlm[ind[3] == 0] = np.load("X_jk%s_lm_%s%s.npy" % (n[1], n[0], n[1])).reshape(1, len(Xlm[ind[3] == 0]))[0]

	# k[0]-k[1] plane
	for c in itertools.product(range(-N_T+1, 1), range(-N_T+1, 1)):
		for i in range(l_X+1):
			Xlm[(ind[1] - ind[2] == -c[0]) & (ind[1] - ind[3] == -c[1])].reshape(hp.sphtfunc.Alm.getsize(l_X), len(Xlm[(ind[1] - ind[2] == -c[0]) & (ind[1] - ind[3] == -c[1])]) / hp.sphtfunc.Alm.getsize(l_X))[i] = Xlm[(ind[1]==0) & ((ind[2]==-c[0]) & (ind[3]==-c[1]))][i]
	# j-k[0], j-k[1] plane
	for c_2 in range(T):
		for c_1 in range(-N_T+c_2+1, c_2+1):
			for i in range(l_X+1):
				Xlm[(ind[1] - ind[2] == -c_1) & (ind[1] - ind[3] == -c_2)].reshape(hp.sphtfunc.Alm.getsize(l_X), len(Xlm[(ind[1] - ind[2] == -c_1) & (ind[1] - ind[3] == -c_2)]) / hp.sphtfunc.Alm.getsize(l_X))[i] = Xlm[(ind[1]==c_2) & ((ind[2]==c_2-c_1) & (ind[3]==0))][i]	# j-k[0]
				Xlm[(ind[1] - ind[2] == -c_2) & (ind[1] - ind[3] == -c_1)].reshape(hp.sphtfunc.Alm.getsize(l_X), len(Xlm[(ind[1] - ind[2] == -c_1) & (ind[1] - ind[3] == -c_2)]) / hp.sphtfunc.Alm.getsize(l_X))[i] = Xlm[(ind[1]==c_2) & ((ind[2]==0) & (ind[3]==c_2-c_1))][i]	# j-k[1] because c_1 <--> c_2

	# contract j
	Xlm = np.einsum("ijkl->ikl", Xlm)

	# rotate baseline into original system
	if n[0] != n[1]:
		baseline = lalsimulation.DetectorPrefixToLALDetector(n[0]).location - lalsimulation.DetectorPrefixToLALDetector(n[1]).location
		baseline = baseline / np.linalg.norm(baseline)
		theta = np.arccos(baseline[2])
		phi = np.arctan2(baseline[1], baseline[0])
		r = hp.rotator.Rotator(rot=(phi,theta,0), deg=False, inv=True, eulertype="Y")
	else:	# n[0] == n[1]
		baseline = lalsimulation.DetectorPrefixToLALDetector(n[0]).location
		baseline = baseline / np.linalg.norm(baseline)
		theta = np.arccos(baseline[2])
		phi = np.arctan2(baseline[1], baseline[0])
		r = hp.rotator.Rotator(rot=(phi,theta,0), deg=False, inv=True, eulertype="Y")

	# TODO: remake below with hp.rotate_alm when it is inplemented
	Xlm = Xlm.transpose(1,2,0)	# Xlm[k[0]][k[1]][lm]
	for k in itertools.product(range(N_T), range(N_T)):
		alm = Xlm[k[0]][k[1]].copy()	# For array ordering
		X = hp.alm2map(alm, NSIDE)
		X = r.rotate_map(X)
		Xlm[k[0]][k[1]] = hp.map2alm(X, lmax=l_X)
	
	Xlm = Xlm.transpose(2,0,1)	# Xlm[lm][k[0]][k[1]]
	
	# save
	np.save("Xlm_%s%s.npy" % (n[0], n[1]), Xlm)
	print n

end = time.time()
print "processing time: " + str(end - start) + " s"
