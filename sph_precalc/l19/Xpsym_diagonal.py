import numpy as np
import matplotlib
#matplotlib.use("Agg")
from matplotlib import pyplot as plt
import healpy as hp
import time
import itertools
from sympy.physics.wigner import gaunt
import ConfigParser

from lal import ComputeDetAMResponse, C_SI
import lalsimulation


start = time.time()
config = ConfigParser.ConfigParser()
config.read('setting.ini')
section = "parameter set"

NSIDE = config.getint(section, "nside")
npix = config.getint(section, "npix")
DetectorName = eval(config.get(section, "detectorname"))
n_ifo = config.getint(section, "n_ifo")
N_T = config.getint(section, "n_t")
l_T = config.getint(section, "l_t")
l_P = config.getint(section, "l_p")
l_X = config.getint(section, "l_x")
T = config.getint(section, "t")


#
# =======================================================================
#
#				calc X
#
# =======================================================================
# j,k_iter

# diagonal part
for n in range(n_ifo):
	Plm = np.load("Psymlm_%s%s.npy" % (str(DetectorName[n]), str(DetectorName[n])))
	X_jk_1 = 1j*np.zeros((hp.sphtfunc.Alm.getsize(l_X), T, N_T))	# j-k[0] plane
	X_jk_2 = 1j*np.zeros((hp.sphtfunc.Alm.getsize(l_X), T, N_T))	# j-k[1] plane
	for l in range(l_X+1):
		# file name
		# ---------
		# element
		for k in range(N_T):
			# true param for X_{\alpha \beta k_1 k_2}^{lm}
			# -----------------------
			# dummy param
			for j in range(-T/2,T/2+1):
				Tlm_1 = np.load("Tsymlm_%s%s_%d.npy" % (str(DetectorName[n]), str(DetectorName[n]), j-((N_T-1)/2-k)))	# k[0] = k
				Tlm_2 = np.load("Tsymlm_%s%s_%d.npy" % (str(DetectorName[n]), str(DetectorName[n]), j- (N_T-1)/2  ))	# k[1] = 0

				for p in range(0, l_X+l_T+1):	# restricted by below if sentense.
					for l_1 in range(abs(l-p)+(l+abs(l-p)+p)&1, min(l_T,l+p)+1, 2):
						if p < abs(l-l_1) or p > abs(l+l_1):
							continue

						for l_p in range(l_P+1):
							for l_2 in range(abs(l_p-p)+(l_p+abs(l_p-p)+p)&1, min(l_T,l_p+p)+1, 2):
								if p < abs(l_p-l_2) or p > abs(l_p+l_2):
									continue

								X_jk_1[hp.sphtfunc.Alm.getidx(l_X,l,0)][j][k] += Plm[hp.sphtfunc.Alm.getidx(l_P,l_p,0)] * Tlm_1[hp.sphtfunc.Alm.getidx(l_T, l_1, 0)] * Tlm_2[hp.sphtfunc.Alm.getidx(l_T, l_2, 0)] * gaunt(l, l_1, p, 0, 0, 0, prec=64) * gaunt(l_p, l_2, p, 0, 0, 0, prec=64)
								X_jk_2[hp.sphtfunc.Alm.getidx(l_X,l,0)][j][k] += Plm[hp.sphtfunc.Alm.getidx(l_P,l_p,0)] * Tlm_2[hp.sphtfunc.Alm.getidx(l_T, l_1, 0)] * Tlm_1[hp.sphtfunc.Alm.getidx(l_T, l_2, 0)] * gaunt(l, l_1, p, 0, 0, 0, prec=64) * gaunt(l_p, l_2, p, 0, 0, 0, prec=64)

			print "jk : l = %d, k = %d" % (l, k)

	np.save("X_jk%s_lm_%s%s.npy" % (DetectorName[n], DetectorName[n], DetectorName[n]), X_jk_1)
	np.save("X_jk%s_lm_%s%s.npy" % (DetectorName[n], DetectorName[n], DetectorName[n]), X_jk_2)



	X_kk = 1j*np.zeros((hp.sphtfunc.Alm.getsize(l_X), N_T, N_T))	# k[0]-k[1] plane
	for l in range(l_X+1):
		# file name
		# ---------
		# element
		for k in itertools.product(range(N_T), range(N_T)):
			# true param for X_{\alpha \beta k_1 k_2}^{lm}
			# -----------------------
			# dummy param
			Tlm_1 = np.load("Tsymlm_%s%s_%d.npy" % (str(DetectorName[n]), str(DetectorName[n]), k[0]-(N_T-1)/2))
			Tlm_2 = np.load("Tsymlm_%s%s_%d.npy" % (str(DetectorName[n]), str(DetectorName[n]), k[1]-(N_T-1)/2))

			for p in range(0, l_X+l_T+1):	# restricted by below if sentense.
				for l_1 in range(abs(l-p)+(l+abs(l-p)+p)&1, min(l_T,l+p)+1, 2):
					if p < abs(l-l_1) or p > abs(l+l_1):
						continue

					for l_p in range(l_P+1):
						for l_2 in range(abs(l_p-p)+(l_p+abs(l_p-p)+p)&1, min(l_T,l_p+p)+1, 2):
							if p < abs(l_p-l_2) or p > abs(l_p+l_2):
								continue

							X_kk[hp.sphtfunc.Alm.getidx(l_X,l,0)][k[0]][k[1]] += Plm[hp.sphtfunc.Alm.getidx(l_P,l_p,0)] * Tlm_1[hp.sphtfunc.Alm.getidx(l_T, l_1, 0)] * Tlm_2[hp.sphtfunc.Alm.getidx(l_T, l_2, 0)] * gaunt(l, l_1, p, 0, 0, 0, prec=64) * gaunt(l_p, l_2, p, 0, 0, 0, prec=64)

			print "kk : l = %d, k[0] = %d, k[1] = %d" % (l, k[0], k[1])

	np.save("X_kk_lm_%s%s.npy" % (DetectorName[n], DetectorName[n]), X_kk)



"""
for n in itertools.combinations_with_replacement(range(n_ifo),2):	# \alpha, \beta
	Plm = np.load("Plm_%s%s.npy" % (str(DetectorName[n[0]]), str(DetectorName[n[1]])))
	X = np.zeros((hp.sphtfunc.Alm.getsize(l_X),N_T,N_T))
	for l in range(l_X+1):
		for m in range(l+1):	# Only positive is OK because the map is real.
			# file name
			# ---------
			# index
			for k in itertools.product(xrange(N_T), xrange(N_T)):	# k_1, k_2
				# true param for X_{\alpha \beta k_1 k_2}^{lm}
				# -----------------------
				# dummy param
				for j in range(-T/2,T/2+1):
					Tlm_1 = np.load("Tlm_%s_%d.npy" % (str(DetectorName[n[0]]), j-k[0]-(N_T-1)/2))
					Tlm_2 = np.load("Tlm_%s_%d.npy" % (str(DetectorName[n[1]]), j-k[1]-(N_T-1)/2))

					for l_p in range(l_X+1):
						for m_p in range(-l_p, l_p+1):
							for l_1 in range(l_X+1):
								for m_1 in range(-l_1, l_1+1):
									for l_2 in range(l_X+1):
										for m_2 in range(-l_2, l_2+1):
											term = 0
											for p in range(l_X):
												factor = 0
												for q in range(l_X):
													factor += wigner_3j(l,l_1,p,m,m_1,q) * wigner_3j(l_p,l_2,p,m_p,m_2,q)

												term += factor * wigner_3j(l,l_1,p,0,0,0) * wigner_3j(l_p,l_2,p,0,0,0) * (2*p+1)

											term *= np.sqrt((2*l+1)*(2*l_1+1)*(2*l_2+1)*(2*l_p+1)) / (4*np.pi)

											if m_p < 0:
												m_p = -m_p
												term *= (-1)**m_p * np.conjugate(Plm[hp.sphtfunc.Alm.getidx(l_X, l_p, m_p)])
											else:
												term *= Plm[hp.sphtfunc.Alm.getidx(l_X, l_p, m_p)]

											if m_1 < 0:
												m_1 = -m_1
												term *= (-1)**m_p * Tlm_1[hp.sphtfunc.Alm.getidx(l_X, l_1, m_1)]
											else:
												term *= np.conjugate(Tlm_1[hp.sphtfunc.Alm.getidx(l_X, l_1, m_1)])

											if m_2 < 0:
												m_2 = -m_2
												term *= (-1)**m_p * np.conjugate(Tlm_2[hp.sphtfunc.Alm.getidx(l_X, l_2, m_2)])
											else:
												term *= Tlm_2[hp.sphtfunc.Alm.getidx(l_X, l_2, m_2)]

											X[hp.sphtfunc.Alm.getidx(l_X,l,m)][k[0]][k[1]] += term

						end = time.time()
						print l_p, m_p, end-start

			np.save("Xlm_%s%s.npy" % (DetectorName[n[0]], DetectorName[n[1]]), X)
"""


end = time.time()
str(end - start) + " s"

