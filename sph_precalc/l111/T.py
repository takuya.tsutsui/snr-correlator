import numpy as np
import cmath
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
from matplotlib import cm as cm
from matplotlib.backends.backend_pdf import PdfPages
import healpy as hp
import time
import itertools

from glue.ligolw import ligolw
from glue.ligolw import lsctables
from glue.ligolw import utils as ligolw_utils
from lal import series as lalseries
from lal import GreenwichMeanSiderealTime, ComputeDetAMResponse, C_SI
import lalsimulation


start = time.time()


#
# =======================================================================
#
#				calc T
#
# =======================================================================
#

NSIDE = 32
npix = hp.nside2npix(NSIDE)
#N_T = 37		# ~peak width
#N_T = 16754136225	# Kipp's estimate, upper limit
N_T = 351		# length of GW170814
dt = 0.00048828125
#l_T = 41001421423	# Kipp's estimate, upper limit
l_T = 111
T_j = 38		# integration duration; for j

DetectorName = ("H1", "L1", "V1")
n_ifo = len(DetectorName)

T = np.zeros(npix)
for name in DetectorName:
	for j in xrange(N_T+T_j):
		for idx in range(npix):
			theta, phi = hp.pix2ang(NSIDE, idx)

			direction = np.array([np.sin(theta)*np.cos(phi), np.sin(theta)*np.sin(phi), np.cos(theta)])
			delay = np.dot(direction, lalsimulation.DetectorPrefixToLALDetector(str(name)).location) / (C_SI * dt)
			delay = int(round(delay))

			T[idx] = np.sinc(j - (N_T-1)/2 - T_j/2 + delay)

		Tlm = hp.sphtfunc.map2alm(T, lmax=l_T)
		np.save("Tlm_%s_%d.npy" % (name, j-(N_T-1)/2-T_j/2), Tlm)



end = time.time()
print str(end - start) + " s"
