import numpy as np
import matplotlib
#matplotlib.use("Agg")
from matplotlib import pyplot as plt
import healpy as hp
import time
import itertools
from sympy.physics.wigner import wigner_3j

from lal import ComputeDetAMResponse, C_SI
import lalsimulation


start = time.time()


#
# =======================================================================
#
#				calc X
#
# =======================================================================
#

NSIDE = 32
npix = hp.nside2npix(NSIDE)
#N_T = 351	# sinc interpolation; for k_1, k_2
N_T = 43
l_T = 65	# upper limit for Time delay operator
l_X = 111	# l_X ~ np.sqrt(npix)|NSIDE=32; for all l
T = 38		# integration duration; for j

DetectorName = ("H1", "L1", "V1")
n_ifo = len(DetectorName)

for n in itertools.combinations_with_replacement(range(n_ifo), 2):
	Plm = np.load("Plm_%s%s.npy" % (str(DetectorName[n[0]]), str(DetectorName[n[1]])))
	X = 1j*np.zeros((hp.sphtfunc.Alm.getsize(l_X),N_T,N_T))
	for l in range(l_X+1):
		for m in range(l+1):	# Only positive is OK because the map is real.
			# file name
			# ---------
			# element
			for k in itertools.product(xrange(N_T), xrange(N_T)):	# k_1, k_2
				# true param for X_{\alpha \beta k_1 k_2}^{lm}
				# -----------------------
				# dummy param
				for j in range(-T/2,T/2+1):
					Tlm_1 = np.load("Tlm_%s_%d.npy" % (str(DetectorName[n[0]]), j-k[0]-(N_T-1)/2))
					Tlm_2 = np.load("Tlm_%s_%d.npy" % (str(DetectorName[n[1]]), j-k[1]-(N_T-1)/2))

					for p in range(l_T+1):
						term = 0
						for l_1 in range(abs(l-p), min(l_T,l+p)+1):
							if (l+l_1+p)%2 == 1:
								continue

							for l_p in range(l_T+1):
								for l_2 in range(abs(l_p-p), min(l_T,l_p+p)+1):
									if (l_p+l_2+p)%2 == 1:
										continue

									factor = 0
									for m_1 in range(max(-l_1,m-p), min(l_1,m+p)+1):
										for q in range(max(-p,m-l_1), min(p,m+l_1)+1):
											for m_p in range(max(-l_p, q-l_2), min(l_p,q+l_2)+1):
												for m_2 in range(max(-l_2,m_p-p), min(l_2, m_p+p)+1):
													if m_p < 0:
														ele = (-1)**m_p * np.conjugate(Plm[hp.sphtfunc.Alm.getidx(l_X,l_p,abs(m_p))])
													else:
														ele = Plm[hp.sphtfunc.Alm.getidx(l_X,l_p,m_p)]

													if m_1 < 0:
														ele *= (-1)**m_p * Tlm_1[hp.sphtfunc.Alm.getidx(l_X, l_1, abs(m_1))]
													else:
														ele *= np.conjugate(Tlm_1[hp.sphtfunc.Alm.getidx(l_X, l_1, m_1)])

													if m_2 < 0:
														ele *= (-1)**m_p * np.conjugate(Tlm_2[hp.sphtfunc.Alm.getidx(l_X, l_2, abs(m_2))])
													else:
														ele *= Tlm_2[hp.sphtfunc.Alm.getidx(l_X, l_2, m_2)]

													factor += ele * wigner_3j(l,l_1,p,m,m_1,q) * wigner_3j(l_p,l_2,p,m_p,m_2,q)


									term += factor * np.sqrt((2*l+1)*(2*l_1+1)*(2*l_2+1)*(2*l_p+1)) * wigner_3j(l,l_1,p,0,0,0) * wigner_3j(l_p,l_2,p,0,0,0)

						X[hp.sphtfunc.Alm.getidx(l_X,l,m)][k[0]][k[1]] += (2*p+1) * term

				print "1 dummy loop"

	X /= 4*np.pi
	np.save("Xlm_%s%s.npy" % (DetectorName[n[0]], DetectorName[n[1]]), X)





"""
for n in itertools.combinations_with_replacement(range(n_ifo),2):	# \alpha, \beta
	Plm = np.load("Plm_%s%s.npy" % (str(DetectorName[n[0]]), str(DetectorName[n[1]])))
	X = np.zeros((hp.sphtfunc.Alm.getsize(l_X),N_T,N_T))
	for l in range(l_X+1):
		for m in range(l+1):	# Only positive is OK because the map is real.
			# file name
			# ---------
			# index
			for k in itertools.product(xrange(N_T), xrange(N_T)):	# k_1, k_2
				# true param for X_{\alpha \beta k_1 k_2}^{lm}
				# -----------------------
				# dummy param
				for j in range(-T/2,T/2+1):
					Tlm_1 = np.load("Tlm_%s_%d.npy" % (str(DetectorName[n[0]]), j-k[0]-(N_T-1)/2))
					Tlm_2 = np.load("Tlm_%s_%d.npy" % (str(DetectorName[n[1]]), j-k[1]-(N_T-1)/2))

					for l_p in range(l_X+1):
						for m_p in range(-l_p, l_p+1):
							for l_1 in range(l_X+1):
								for m_1 in range(-l_1, l_1+1):
									for l_2 in range(l_X+1):
										for m_2 in range(-l_2, l_2+1):
											term = 0
											for p in range(l_X):
												factor = 0
												for q in range(l_X):
													factor += wigner_3j(l,l_1,p,m,m_1,q) * wigner_3j(l_p,l_2,p,m_p,m_2,q)

												term += factor * wigner_3j(l,l_1,p,0,0,0) * wigner_3j(l_p,l_2,p,0,0,0) * (2*p+1)

											term *= np.sqrt((2*l+1)*(2*l_1+1)*(2*l_2+1)*(2*l_p+1)) / (4*np.pi)

											if m_p < 0:
												m_p = -m_p
												term *= (-1)**m_p * np.conjugate(Plm[hp.sphtfunc.Alm.getidx(l_X, l_p, m_p)])
											else:
												term *= Plm[hp.sphtfunc.Alm.getidx(l_X, l_p, m_p)]

											if m_1 < 0:
												m_1 = -m_1
												term *= (-1)**m_p * Tlm_1[hp.sphtfunc.Alm.getidx(l_X, l_1, m_1)]
											else:
												term *= np.conjugate(Tlm_1[hp.sphtfunc.Alm.getidx(l_X, l_1, m_1)])

											if m_2 < 0:
												m_2 = -m_2
												term *= (-1)**m_p * np.conjugate(Tlm_2[hp.sphtfunc.Alm.getidx(l_X, l_2, m_2)])
											else:
												term *= Tlm_2[hp.sphtfunc.Alm.getidx(l_X, l_2, m_2)]

											X[hp.sphtfunc.Alm.getidx(l_X,l,m)][k[0]][k[1]] += term

						end = time.time()
						print l_p, m_p, end-start

			np.save("Xlm_%s%s.npy" % (DetectorName[n[0]], DetectorName[n[1]]), X)
"""




end = time.time()
print str(end - start) + " s"
