import numpy as np
import matplotlib
#matplotlib.use("Agg")
from matplotlib import pyplot as plt
import healpy as hp
import time
import itertools
from sympy.physics.wigner import wigner_3j

from lal import ComputeDetAMResponse, C_SI
import lalsimulation
from lalburst.snglcoinc import TOATriangulator


@lsctables.use_in
class ContentHandler(lalseries.PSDContentHandler):
	pass

start = time.time()


#
# =======================================================================
#
#				parse data
#
# =======================================================================
#

xmldoc = ligolw_utils.load_filename("coinc.xml", contenthandler=ContentHandler, verbose=True)	# change the file name ###

sngl_inspiral_table = lsctables.SnglInspiralTable.get_table(xmldoc)
DetectorName = tuple(sngl_inspiral_table.getColumnByName("ifo"))
n_ifo = len(DetectorName)

data_tree = np.array([xmldoc.getElementsByTagName(ligolw.LIGO_LW.tagName)[index] for index in range(n_ifo)])
	# data_tree[index_of_detector].array[time,real,complex]
cSNR = np.array([lalseries.parse_COMPLEX8TimeSeries(data_tree[index]) for index in range(n_ifo)])
	# correspond the order of DetectorName to the order of cSNR


#
# =======================================================================
#
#			zero padding data
#
# =======================================================================
#

#
# set initial time of data
#
initial_time = np.array([cSNR[index].epoch for index in range(n_ifo)])	# HIDE FOR INJECTION

#
# arrange data along time
#
far_IFO = np.argmax(initial_time)
near_IFO = np.argmin(initial_time)
for i in range(n_ifo):				# TODO:switch up to the number of IFO
	if i!=far_IFO and i!=near_IFO:
		medium_IFO = i

gap = int(round((initial_time[far_IFO] - initial_time[near_IFO]) / cSNR[0].deltaT))	# always positive, deltaT is same
gap_r = int(round((initial_time[far_IFO] - initial_time[medium_IFO]) / cSNR[0].deltaT))
gap_l = int(round((initial_time[medium_IFO] - initial_time[near_IFO]) / cSNR[0].deltaT))

ZeroPad_cSNR = [np.array([]) for x in range(n_ifo)]
ZeroPad_cSNR[far_IFO] = np.insert(cSNR[far_IFO].data.data, 0, np.zeros(gap))
ZeroPad_cSNR[near_IFO] = np.insert(cSNR[near_IFO].data.data, len(cSNR[near_IFO].data.data), np.zeros(gap))
ZeroPad_cSNR[medium_IFO] = np.insert(cSNR[medium_IFO].data.data, 0, np.zeros(gap_l))
ZeroPad_cSNR[medium_IFO] = np.insert(ZeroPad_cSNR[medium_IFO], len(ZeroPad_cSNR[medium_IFO]), np.zeros(gap_r))

#
# reshape data around the time of arival at geocenter
#
N_T = 127	# sinc interpolation; for k_1, k_2

triangulator = TOATriangulator([lalsimulation.DetectorPrefixToLALDetector(str(name)) for name in DetectorName], 0.005*np.ones(n_ifo))
toa = round(int((triangulator(initial_time)[1] - min(initial_time)) / cSNR[0].deltaT))
for i in range(n_ifo):
	ZeroPad_cSNR[i] = ZeroPad_cSNR[i][toa-(N_T-1)/2:toa+(N_T-1)/2]


#
# =======================================================================
#
#				calc L
#
# =======================================================================
#

NSIDE = 32
l_X = 19	# l_X ~ np.sqrt(npix)|NSIDE=32; for all l
T = 38		# integration duration; for j

logLlm = np.zeros(hp.sphtfunc.Alm.getsize(l_X))

# offdiagonal part
for n in itertools.conbinations(range(n_ifo), 2):
	Xlm = np.load("Xlm_%s%s.npy" % (str(DetectorName[n[0]]), str(DetectorName[n[1]])))
	logLlm += np.dot(np.dot(Xlm, Zeropad_cSNR[n[0]]), np.conjugate(Zeropad_cSNR[n[1]])) + np.dot(np.dot(Xlm, Zeropad_cSNR[n[1]]), np.conjugate(Zeropad_cSNR[n[0]]))

# diagonal part
for n in range(n_ifo):
	Xlm = np.load("Xlm_%s%s.npy" % (str(DetectorName[n]), str(DetectorName[n])))
	logLlm += np.dot(np.dot(Xlm, Zeropad_cSNR[n]), np.conjugate(Zeropad_cSNR[n]))

logLlm *= T/2.
logL = hp.sphtfunc.alm2map(logLlm, NSIDE)


#
# =======================================================================
#
#				plot
#
# =======================================================================
#

hp.mollview(logL, rot=(180,0,0), title="Liklihood ratio of GW source location", unit="log(Likelihood ratio)")
hp.graticule()
plt.savefig("map.pdf")


end = time.time()
print str(end - start) + " s"
